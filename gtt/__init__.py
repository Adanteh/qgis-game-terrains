from qgis.gui import QgisInterface
from gtt.plugin import GameTerrainTools

def classFactory(iface: QgisInterface):
    """Plugin entry point"""
    return GameTerrainTools(iface)
