from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QToolBar, QAction
from PyQt5.QtGui import QIcon

from gtt.functions import FOLDER
from gtt.tools import GttTools
from gtt.ui.widget import GttWidget

class GameTerrainTools:
    """QGIS Plugin Implementation."""

    NAME = "GameTerrainTools"

    def __init__(self, iface):
        """Constructor"""
        iface.gtt_plugin = self

        self.menu = "&Game Terrain Tools"
        self.iface = iface
        self.widgets = {}
        self.actions = []

        child = iface.mainWindow().findChild(QToolBar, self.NAME)
        if child is None:
            self.toolbar = self.iface.addToolBar("Game Terrain Tools")
            self.toolbar.setObjectName(self.NAME)
        else:
            self.toolbar = child
        self.tools = GttTools()

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        self.add_action("icon.png", text="Game Terrains", callback=self.run, parent=self.iface.mainWindow())

    # --------------------------------------------------------------------------
    def widget(self, tag):
        try:
            return self.widgets[tag]
        except KeyError:
            return None

    def widgetCreate(self, tag, widget_class, **kwargs):
        if tag in self.widgets.keys():
            return None
        else:
            widget = widget_class(parent=self.iface.mainWindow(), **kwargs)
            widget.closeWidget.connect(self.widgetClose)
            self.iface.addDockWidget(Qt.RightDockWidgetArea, widget)
            self.widgets[tag] = widget
            widget.show()

    def widgetClose(self, tag):
        widget = self.widgets[tag]
        widget.deleteLater()
        self.iface.removeDockWidget(widget)
        del self.widgets[tag]

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""

        widgets = [self.widgets[key] for key in self.widgets.keys()]
        for widget in widgets:
            widget.close()

        self.tools.unload()
        for action in self.actions:
            self.iface.removePluginMenu(self.menu, action)
            self.iface.removeToolBarIcon(action)

        del self.toolbar
        del self.tools

    def run(self):
        """Run method that loads and starts the plugin"""
        self.widgetCreate("primary", GttWidget, tools=self.tools)

    def add_action(self, icon_path: str, text: str, callback: callable, parent: QWidget = None,) -> QAction:
        """Add toolbar and plugin menu QAction"""
        icon = QIcon(str(FOLDER / "data" / icon_path))
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(True)

        self.toolbar.addAction(action)
        self.iface.addPluginToMenu(self.menu, action)
        self.actions.append(action)
        return action
