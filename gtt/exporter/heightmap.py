import os
import subprocess

from functools import partial
from pathlib import Path
from numpy import loadtxt, savetxt

from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

from qgis import core
from qgis.core import QgsProject, QgsApplication, QgsProcessingAlgRunnerTask
from qgis.core import QgsRasterLayer, QgsTask
from qgis.utils import iface

from gtt.functions import get_layer, save_location, show_progress, qgis_bin_folder, message_log
from gtt.functions import get_uid
from gtt.functions import get_temp_path, FOLDER
from gtt.engine.arma import EngineArma
from gtt.extent import transform_extent, extent_utm_zone
from gtt.process import background_process


def step():
    layer = get_layer("gtt_heightmap")
    bounds = transform_extent(extent_utm_zone())
    resampler = iface.gtt.exporter.resampler  # type: Resampler
    resampler.run(bounds)
    resampler.resample_heightmap_01(layer, add=True)


def fix():
    layer = get_layer("gtt_heightmap")
    bounds = transform_extent(extent_utm_zone())
    resampler = iface.gtt.exporter.resampler  # type: Resampler
    resampler.run(bounds)
    resampler.first_run_fixer()


def rescale():
    layer = get_layer("heightmap_02_5d10e0b59eb3e")
    bounds = transform_extent(extent_utm_zone())
    resampler = iface.gtt.exporter.resampler  # type: Resampler
    resampler.run(bounds)
    resampler.resample_heightmap_04(layer, add=True)


class Resampler(QObject):
    """
        Heightmap resampler
        Runs multiple GDAL processing scripts in a proper order,
        to give it the resolution we want, with the bounds we want

        Steps:
            1) Change projection to localized UTM, crop to bounds + 10%
            2) Merge resulting cropped maps
            3) Resamples to correct resolution (terrain size / heightmap resolution)
            4) Crops to bounds

    """

    done = pyqtSignal(object)
    error_signal = pyqtSignal(object)
    progressChanged = pyqtSignal(float)

    ouput: Path
    task: QgsTask

    def __init__(self, settings, parent=None):
        super().__init__(parent=parent)

        self.settings = settings
        self.filefolder = str(save_location())

        self.resample_gen = None
        self._generated = {}
        self._run = 0
        self._layers = []
        self._lastlayer = None
        self.task = None
        self.output = None
        self.error = None
        self.uid = "123"

        # Bug circumvention, check first_run_fixer docstring
        if not hasattr(iface, "GttResampler_firstrun"):
            iface.GttResampler_firstrun = True

    def run(self, bounds: tuple):
        """Prepares the resampling for a new run, must always be run before running any method
        
        Args:
            bounds (tuple): Coordinates of the extent
        """
        self.extents = []
        self.output = None  # Make sure to reset this, so we can detect actual output

        # TODO ROTATION
        left, bottom, right, top = bounds
        right = left + (self.settings["mapsize"])
        top = bottom + (self.settings["mapsize"])

        self.bounds = (left, bottom, right, top)
        self.progress = show_progress("Processing heightmap", range_=(0, 100))
        self.uid = get_uid()

    @pyqtSlot(str)
    def _done(self, filepath):
        self.progress.setText("Finished processing heightmap")
        self.output = filepath
        self.done.emit(self)

    def _error(self):
        self.error = "Heightmap processing error. Check Processing tab in Log Messages panel"
        self.progress.setText("Failed processing heightmap")
        iface.messageBar().pushCritical("EXPORT ERROR", "Heightmap processing error")
        self.error_signal.emit(self)

    def first_run_fixer(self):
        """
            For some reason first run of this after starting the program will never function properly with extent
            Instead we just call this an extra time at the start with the assumption that it will fail
        """

        def cleanup(layer):
            QgsProject.instance().removeMapLayers([layer.id()])

        layer = QgsRasterLayer(str(FOLDER / "data" / "heightmap_sample.tiff"), "gtt_temp_layer")
        task = self.resample_heightmap_01(layer, add=False)
        task.executed.connect(lambda: cleanup(layer))

    def alg_done(self, filepath, add, results):
        iface._result = results
        filepath_obj = Path(filepath)
        if not filepath_obj.exists():
            self._error()
            return

        layer = QgsRasterLayer(filepath, filepath_obj.stem)
        if add:
            QgsProject.instance().addMapLayer(layer)

        self._lastlayer = layer
        if self.resample_gen is not None:
            self._run += 1
            try:
                self.resample_gen.send(layer)
            except StopIteration:
                self.resample_gen.close()
                self._done(filepath)

    def resample_generator(self):
        self._layers = QgsProject.instance().mapLayersByName("gtt_heightmap")
        self.layerscut = []
        try:

            # Fix the first run of this always failing after program startup
            # 2019-04-03
            if iface.GttResampler_firstrun:
                iface.GttResampler_firstrun = False
                yield self.first_run_fixer()

            for layer in self._layers:
                layer_result = yield self.resample_heightmap_01(layer)
                self.layerscut.append(layer_result)
            if len(self.layerscut) > 1:
                layer = yield self.merge_heightmaps(self.layerscut)
            else:
                layer = self.layerscut[0]

            layer = yield self.resample_heightmap_02(layer)
            layer = yield self.resample_heightmap_03(layer, True)
        except GeneratorExit:
            pass

    def resample_complete(self, add=True):
        self.resample_gen = self.resample_generator()
        self.resample_gen.__next__()

    def adjust_bounds(self, bounds, margin=0.1):
        """
            Add a 10% margin, cutting to the correct bounds right away can cause issues with lack of resolution
            By reducing the overal size it'll be faster (performance + disk space) to resample in the next step
        """
        left, bottom, right, top = bounds
        width = right - left
        height = top - bottom
        left = left - (width * margin)
        bottom = bottom - (height * margin)
        right = right + (width * margin)
        top = top + (height * margin)
        return (left, bottom, right, top)

    def merge_heightmaps(self, layers, add=False):
        filepath = get_temp_path() / ("heightmap_merged_" + self.uid + ".tiff")

        parameters = {
            "INPUT": layers,
            "DATA_TYPE": 6,  # Float32
            "OUTPUT": str(filepath),
        }

        _msg = "Heightmap merge"
        self.progress.setText(_msg)
        message_log(_msg)
        return self.add_task("gdal:merge", parameters, add)

    def merge_heightmaps_virtual(self, layers, add=False):
        filepath = get_temp_path() / ("heightmap_merged_" + self.uid + ".tiff")

        parameters = {
            "INPUT": layers,
            "RESOLUTION": 1,
            "SEPARATE": False,
            "RESAMPLING": 2,
            "OUTPUT": str(filepath),
        }

        _msg = "Heightmap merge"
        self.progress.setText(_msg)
        message_log(_msg)
        return self.add_task("gdal:buildvirtualraster", parameters, add)

    def resample_heightmap_01(self, layer: QgsRasterLayer, add=False):
        """Converts projection, clips with a general margin"""

        filepath = get_temp_path() / ("heightmap_01_" + get_uid() + ".tiff")

        utm_id = extent_utm_zone()
        target_crs = core.QgsCoordinateReferenceSystem(utm_id)
        extent_tuple = self.adjust_bounds(self.bounds)

        extent = core.QgsRectangle(*extent_tuple)
        self.extents.append(extent)

        parameters = {
            "INPUT": layer,
            "SOURCE_CRS": layer.crs(),
            "TARGET_CRS": target_crs,
            "RESAMPLING": 2,  # Cubic
            "OUTPUT": str(filepath),
            "NODATA": -9999,
            "DATA_TYPE": 6,  # Float32
            "TARGET_EXTENT": extent,
            "TARGET_EXTENT_CRS": target_crs,
        }

        _msg = "Heightmap resample 1: Fixing projection, bit depth, cropping"
        self.progress.setText(_msg)
        message_log(_msg)

        return self.add_task("gdal:warpreproject", parameters, add)

    def resample_heightmap_02(self, layer: QgsRasterLayer, add=False):
        """Resamples to the wanted resolution"""

        crs = layer.crs()
        filepath = get_temp_path() / ("heightmap_02_" + self.uid + ".tiff")
        resolution = self.settings["mapsize"] / self.settings["height_reso"]

        parameters = {
            "INPUT": layer,
            "SOURCE_CRS": crs,
            "TARGET_CRS": crs,
            "RESAMPLING": 2,  # Cubic
            "NODATA": -9999,
            "OUTPUT": str(filepath),
            "TARGET_RESOLUTION": resolution,
            "EXTRA": "-wo SOURCE_EXTRA=1000",
        }

        _msg = "Heightmap resample 2: Scaling to wanted resolution"
        self.progress.setText(_msg)
        message_log(_msg)
        return self.add_task("gdal:warpreproject", parameters, add)

    def resample_heightmap_03(self, layer: QgsRasterLayer, add=False):
        """Clips to the actual bounds"""

        crs = layer.crs().authid()
        filepath = get_temp_path() / ("heightmap_03_" + self.uid + ".tiff")

        left, bottom, right, top = self.bounds
        extent = core.QgsRectangle(left, bottom, right, top)

        parameters = {
            "INPUT": layer,
            "SOURCE_CRS": crs,
            "TARGET_CRS": crs,
            "RESAMPLING": 2,  # Cubic
            "OUTPUT": str(filepath),
            "NODATA": -9999,
            # 'DATA_TYPE': 1,  # Int16
            "TARGET_EXTENT": extent,
            "TARGET_EXTENT_CRS": crs,
            "EXTRA": "-wo SOURCE_EXTRA=1000",
        }

        _msg = "Heightmap resample 3: Cropping to marked area"
        self.progress.setText(_msg)
        message_log(_msg)
        return self.add_task("gdal:warpreproject", parameters, add)

    def resample_heightmap_04(self, layer: QgsRasterLayer, add=False):
        """Overwrites the extent if required"""

        if self.settings["scaled"]:
            scalefactor = self.settings["scaled_size"] / self.settings["mapsize"]
            left, bottom, right, top = self.bounds
            # right = left + (self.settings['mapsize'] * scalefactor)
            # top = bottom + (self.settings['mapsize'] * scalefactor)

            path = layer.source()
            output = get_temp_path() / ("heightmap_04_" + self.uid + ".tiff")
            crs = layer.crs().authid()

            cmd = [
                str(qgis_bin_folder() / "gdal_translate.exe"),
                "-of",
                "GTiff",
                "-a_srs",
                crs,
                str(path),
                str(output),
                "-a_ullr",
                f"{left}",
                f"{top}",
                f"{right}",
                f"{bottom}",
            ]
            return_code = background_process(cmd, heartbeat=True)
            if return_code == 0:
                layer = QgsRasterLayer(str(output), output.stem)
                if add:
                    QgsProject.instance().addMapLayer(layer)

    def add_task(self, alg, parameters, add):
        alg = QgsApplication.processingRegistry().algorithmById(alg)
        self.task = QgsProcessingAlgRunnerTask(alg, parameters, iface.gtt.context, iface.gtt.feedback)
        self.task.executed.connect(partial(self.alg_done, parameters["OUTPUT"], add))
        self.task.progressChanged.connect(self.progress.setProgress)
        QgsApplication.taskManager().addTask(self.task)
        return self.task
