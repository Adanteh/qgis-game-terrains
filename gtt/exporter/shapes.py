from PyQt5.QtCore import QVariant

from qgis.utils import iface
from qgis.core import Qgis
from qgis.core import (
    QgsField,
    QgsFields,
    QgsVectorFileWriter,
    QgsCoordinateReferenceSystem,
    QgsFeature,
)
from PyQt5.QtGui import QTransform

from ..functions import (
    get_layer,
    save_location,
    create_transform_layer,
    message_log,
)
from ..extent import transform_extent
from ..engine.base import Engine

NAME = "osm multipolygons"

def export_shapes(engine: Engine, extent_crs="EPSG:32631", parent=None):
    layer = get_layer(NAME)
    if layer is None:
        iface.messageBar().pushCritical("Missing Layer", f"No layer called '{NAME}' found for shapes export")
        message_log("To export shapes, please use the download option first", level=Qgis.Warning)
        return None
    features = layer.getFeatures()

    featurearray = [feature for feature in features]
    if len(featurearray) == 0:
        return 0

    wkbtype = featurearray[0].geometry().wkbType()
    exportpath = str(save_location() / "shapes.shp")

    target_crs = QgsCoordinateReferenceSystem(extent_crs)
    export_crs = QgsCoordinateReferenceSystem(engine.CRS)
    fields = QgsFields()
    fields.append(QgsField("TYPE", QVariant.String))
    fields.append(QgsField("SUBTYPE", QVariant.String))
    writer = QgsVectorFileWriter(exportpath, "utf-8", fields, wkbtype, export_crs, "ESRI Shapefile")

    transform = create_transform_layer(layer, target_crs=extent_crs)
    left, bottom, _, _ = transform_extent(targetcrs=extent_crs)
    extent_translate = (engine.COORD_X - left, engine.COORD_Y + bottom * -1)

    if not iface.gtt.settings["scaled"]:
        scale = 1.0
    else:
        scale = iface.gtt.settings["scaled_size"] / iface.gtt.settings["mapsize"]

    transform_scale = QTransform()
    transform_scale.scale(scale, scale)
    transform_scale.translate(engine.COORD_X, engine.COORD_Y)

    message_log(f"Translate shape coords by: {extent_translate}")

    # Copy the roads we want
    for feature in layer.getFeatures():
        if not feature["_TYPE"] or feature["_TYPE"] == "unknown":
            continue

        geom = feature.geometry()  # type: QgsGeometry
        geom.transform(transform)
        geom.translate(extent_translate[0], extent_translate[1], 0)  # Set them to arma's mapframe
        if scale != 1.0:
            geom.transform(transform_scale)

        featurenew = QgsFeature()
        featurenew.setGeometry(geom)
        featurenew.setAttributes([feature["_TYPE"], feature["_SUBTYPE"]])
        writer.addFeature(featurenew)

    del writer
