from functools import partial
from pathlib import Path

from PyQt5.QtWidgets import QProgressBar, QMessageBox
from PyQt5.QtCore import Qt, QObject, QSize, QSettings, pyqtSlot, pyqtSignal
from PyQt5.QtGui import QColor

from qgis.utils import iface
from qgis.core import Qgis, QgsProject, QgsRasterLayer, QgsApplication
from qgis.core import QgsMapLayerStore

from gtt.functions import get_layer, project_folder, show_progress, hide_progress, message_log, SafeProgressBar
from gtt.extent import transform_extent, extent_utm_zone
from gtt.satmap import satmap_zoomed
from gtt.classification import export as otb_export

from .satmap import Printer
from .roads import export_roads
from .shapes import export_shapes
from .heightmap import Resampler
from .details import export_details

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from gtt.ui.widget import GttWidget

layoutmanager = QgsProject.instance().layoutManager()
layout = layoutmanager.layoutByName("gtt_mask")



class GttExport(QObject):
    """

        The main export functionality. This is setup in a QObject to support the QgsTask system.
        This will export everything that is set to the GttWidget.tools.settings dictionary.
    """

    continue_export = pyqtSignal()
    progressChanged = pyqtSignal(int)
    progress: SafeProgressBar

    def __init__(self, tools: "GttWidget", parent=None):
        super().__init__(parent=parent)
        self.tools = tools
        self.progress = None
        self.export_types = []
        self.export_count = 0
        self.continue_export.connect(self._continue)

        # -------------------------------------------------------------------------------
        # Raster printer
        self.printer = Printer(tools.settings, parent=self)
        self.printer.progressChanged.connect(self._progress)
        self.printer.done.connect(self._donewrapper)

        # -------------------------------------------------------------------------------
        # Heightmap resampling
        self.resampler = Resampler(tools.settings, parent=self)
        self.resampler.done.connect(self.exportEngine)
        self.resampler.error_signal.connect(self._donewrapper)

    def export(self, types=["satmap", "mask", "height", "roads"]):
        """
        Export data

        Arg:
            List of which data types to export
        """

        if not types:
            iface.messageBar().pushCritical("Failed export", "No datatypes were selected for export")
            return 0

        if str(project_folder()) == ".":
            iface.messageBar().pushCritical("Export error", "Please save the QGIS Project before attempting export")

        self.export_types = types
        self.export_count = len(types)
        self._continue()

    def _continue(self, *args, **kwargs):
        try:
            hide_progress(self.progress)
        except RuntimeError:
            pass

        if not self.export_types:
            export_details(self.tools.settings)
            message_log("<b>Export done!</b>")
            iface.messageBar().pushSuccess("Export done", "Finished the export")
            self.progressChanged.emit(100)
            return
            # DONE

        progress = int((1 - (len(self.export_types) / self.export_count)) * 100)
        self.progressChanged.emit(progress)

        export_type = self.export_types.pop(0)
        self.progress = show_progress("Exporting " + export_type, range_=(0, 100))

        if "satmap" == export_type:
            self.exportSatmap("gtt_satmap")
        elif "mask" == export_type:
            self.exportMask("gtt_mask_osm")
        elif "roads_raster" == export_type:
            self.exportRoadsRaster('gtt_roads_raster')
        elif "classified mask" == export_type:
            self.exportMaskClassified("gtt_mask")
        elif "height" == export_type:
            self.exportHeightmap("gtt_height")
        elif "roads" == export_type:
            export_roads(self.tools.engine, extent_utm_zone(), parent=self)
            self._continue()
        elif "shapes" == export_type:
            export_shapes(self.tools.engine, extent_utm_zone(), parent=self)
            self._continue()

    def _progress(self, progress: int):
        try:
            if self.progress is not None:
                self.progress.setRange(0, 100)
                self.progress.setProgress(progress)
        except RuntimeError:
            pass

    def _exit(self):
        hide_progress(self.progress)
        message_log("<b>EXPORT TERMINATED</b>")

    def exportSatmap(self, exportname: str):
        """Exports the satmap in a bmp"""

        layer = get_layer("gtt_satmap")
        if layer is None:
            iface.messageBar().pushCritical("Missing Layer", "No layer called 'gtt_satmap' found for satmap export")
            message_log(
                "To export satmap, please make sure a layer called 'gtt_satmap' exists",
                level=Qgis.Warning,
            )
            self._exit()
            return 1

        if not self.tools.engine.TILED_EXPORT and self.tools.settings["tilesize_enabled"]:
            message = "Tiled export is not supported for this engine"
            iface.messageBar().pushCritical("Satmap export", message)
            message_log(message, level=Qgis.Warning)
            self._exit()
            return 1

        layer = satmap_zoomed(layer, self.tools.settings["zoom_level"], name="gtt_satmap_temp")
        extension = self.tools.engine.SATMAP_FORMAT
        exporting = self.printer.export(layer, exportname=exportname, aliasing=True, extension=extension)
        if not exporting:
            self._exit()
            return 1

        return 0

    def exportMaskClassified(self, exportname: str):
        """Exports mask in a bmp"""

        task = otb_export(exportname)
        if isinstance(task, int):
            self._exit()
            return task

        self.task = task
        self.task.final.connect(self._donewrapper)
        return 0

    def exportMask(self, exportname):
        """Exports mask (based on osm multipolygons layer) in a bmp"""

        if not self.tools.engine.MASK_SUPPORT:
            message = "Mask export is currently not supported for this engine"
            iface.messageBar().pushCritical("Mask export", message)
            message_log(message, level=Qgis.Warning)
            self._exit()
            return 1

        layer = get_layer("osm multipolygons")
        if layer is None:
            iface.messageBar().pushCritical("Mask export", "Download OSM data before exporting mask")
            message_log(
                "To export mask, please make sure a layer called 'osm multipolygons' exists",
                level=Qgis.Warning,
            )
            self._exit()
            return 1

        def _set_background(printer: Printer):
            printer.itemmap.setBackgroundColor(QColor(128, 197, 46, 255))

        extension = self.tools.engine.SATMAP_FORMAT
        exporting = self.printer.export(layer, exportname=exportname, callback=_set_background, extension=extension)
        if not exporting:
            self._exit()
            return 1

        return 0

    def exportRoadsRaster(self, exportname: str) -> int:
        """Exports the roads (based on osm roads layer) in a bmp. Main usage is for further processing"""

        layer = get_layer("osm lines")
        if layer is None:
            iface.messageBar().pushCritical("Mask export", "Download OSM data before exporting mask")
            message_log("To export roads, please make sure an osm roads layer exists", level=Qgis.Warning)
            self._exit()
            return 1
        
        def __set_background(printer: Printer):
            printer.itemmap.setBackgroundColor(QColor(0, 0, 0, 255))

        extension = self.tools.engine.SATMAP_FORMAT
        exporting = self.printer.export(layer, exportname=exportname, callback=__set_background, extension=extension)
        if not exporting:
            self._exit()
            return 1

        return 0

    def exportHeightmap(self, exportname):
        """Resamples the heightmap"""

        layer = get_layer("gtt_heightmap")
        if layer is None:
            iface.messageBar().pushCritical(
                "Missing Layer",
                "No layer called 'gtt_heightmap' found for heightmap export",
            )
            message_log(
                "To export heightmap, please make sure a layer called 'gtt_heightmap' exists",
                level=Qgis.Warning,
            )
            self._exit()
            return 1

        if isinstance(layer, QgsRasterLayer):
            bounds = transform_extent(extent_utm_zone())
            self.resampler.run(bounds)
            self.resampler.resample_complete(add=True)  # Emits done(self) when finished
            return 0

    def exportEngine(self, resampler: Resampler):
        """
        Calls the selected engine final export task. This will handle the final export format,
        internally we work with georeferenced GTiffs for everything

        Args:
            resampler (Resampler): [description]
        """
        if resampler.output is not None:
            hide_progress(resampler.progress)

            task = self.tools.engine.heightExport(resampler.output)
            if task is not None:
                self.task = task
                self.task.done.connect(self._donewrapper)
                QgsApplication.taskManager().addTask(self.task)

    @pyqtSlot(object)
    def _donewrapper(self, object_):
        if object_.output is not None:
            self.continue_export.emit()
        else:
            if object_.error is not None:
                message_log(f"Error: {object_.error}", level=Qgis.Critical)
                self._exit()
