"""
    Exporter for satmap, uses print manager 
"""
import os
import math
from pathlib import Path

from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QPoint, QRect, Qt
from PyQt5.QtGui import QImage, QPainter

from qgis.core import (
    Qgis,
    QgsUnitTypes,
    QgsProject,
    QgsApplication,
    QgsPrintLayout,
    QgsLayoutItemMap,
    QgsLayoutManager,
    QgsMapLayer,
)
from qgis.core import QgsLayoutExporter, QgsLayoutRenderContext, QgsLayoutSize
from qgis.core import QgsCoordinateReferenceSystem, QgsRectangle, QgsTask
from qgis.utils import iface

from gtt.functions import get_layer, save_location, message_log
from gtt.extent import transform_extent, extent_utm_zone


def export_tile(x=0, y=0, layer="satmap", path: Path = None):
    """Test export for single tile
    
    Args:
        x (int, optional): Column. Defaults to 0.
        y (int, optional): Row. Defaults to 0.
        layer (str, optional): layer name, without prefix. Defaults to 'satmap'.
        path (Path, optional): Export path, defaults to project location
    """
    printer = Printer(iface.gtt.settings, iface.gtt)
    printer.prepare(get_layer(f"gtt_{layer}"), exportname="test_export", path=path)
    printer.cleanup = False
    printer.handleTile(column=x, row=y)


def export_full(layer="satmap", path: Path = None):
    """Exports entire map
    
    Args:
        layer (str, optional): layer name, without prefix. Defaults to 'satmap'.
        path (Path, optional): Export path, defaults to project location
    """
    printer = Printer(iface.gtt.settings, iface.gtt)
    printer.export(get_layer(f"gtt_{layer}"), exportname="test_export", path=path)


def merge_tiles(layer="satmap", path: Path = None):
    """Executes the merging and tiled export function with already exported tileset
    
    Args:
        layer (str, optional): [description]. Defaults to 'satmap'.
        path (Path, optional): [description]. Defaults to project location.
    """

    printer = Printer(iface.gtt.settings, iface.gtt)
    printer.prepare(get_layer(f"gtt_{layer}"), exportname="test_export", path=path)

    if path is None:
        path = save_location()

    for file in path.glob(f"gtt_*_*{printer.extension}"):
        parts = file.stem.split("_")
        printer.tiles.append((file, int(parts[1]), int(parts[2])))

    printer.mergeTiles()


class Printer(QObject):
    done = pyqtSignal(object)
    progressChanged = pyqtSignal(int)
    TEMPSIZE = 4096

    savepath: Path
    output: QImage
    name: str
    aliasing: bool
    rows: int
    columns: int
    mapsize: int
    layoutmanager: QgsLayoutManager

    def __init__(self, settings, parent=None):
        super().__init__(parent=parent)
        self.extension = ".bmp"
        self.settings = settings
        self.savepath = None
        self.rows = 0
        self.columns = 0
        self.tiles = []
        self.mapsize = settings["mapsize"]
        self.layoutmanager = QgsProject.instance().layoutManager()
        self.output = None
        self.error = None
        self.aliasing = False  # Antialiasing print

        self._list = None
        self._generator = None
        self._task = None

    @pyqtSlot()
    def _done(self):
        self.done.emit(self)

    def export(self, layer: QgsMapLayer, exportname=None, callback=None, path: Path = None, extension="", **kwargs):

        # Set which extension we'd like to export with
        if extension:
            self.extension = extension

        if not self.validate():
            return False

        self.prepare(layer, exportname, path=path, **kwargs)
        self.tiles = []

        # QGIS doesn't like exporting pictures of 10k in size, so to be safe,
        # we'll always export 4k tiles and then patch them together manually
        self.rows = self.tilecount
        self.columns = self.tilecount
        self._list = []

        if callback is not None:
            callback(self)

        for column in range(0, self.columns):
            for row in range(0, self.rows):
                self._list.append((column, row))

        self._generator = self.handleTiles()
        self._generator.__next__()
        return True

    def handleTiles(self):
        self._i = 0
        for column, row in self._list:
            task: QgsTask = self.handleTile(column, row)
            task.taskCompleted.connect(self._next)
            task.taskTerminated.connect(self._done)
            yield

    def _next(self):
        try:
            self._i += 1
            progress = int(self._i / (self.tilecount ** 2) * 100)
            self.progressChanged.emit(progress)
            self._generator.__next__()
        except StopIteration:
            self.output = self.mergeTiles()
            self._done()

    def export_single(self, layer, column: int = 0, row: int = 0):
        """Debug function to test basic printing for a single function. Not used in production"""
        self.prepare(layer)
        self.handleTile(column, row)

    def validate(self, quiet=False) -> bool:
        """Validates if current export settings are correct
        
        Returns:
            bool: True if correct
        """

        size = self.settings["mapsize"]
        if self.settings["scaled"]:
            size = self.settings["scaled_size"]

        if self.settings["map_reso"] != -1:
            size = self.settings["map_reso"]

        # Maximum size for a BMP image
        if size > 32766:
            if not self.settings["tilesize_enabled"]:
                if not quiet:
                    iface.messageBar().pushCritical(
                        "EXPORT", "Maximum 32766px size allowed, please use tiling for maps above this size",
                    )

                return False
        return True

    def prepare(self, layer: QgsMapLayer, exportname: str = None, path: Path = None,**kwargs):
        """
            Called before any export function (Single or multiple)
            Seeing this object iself is persistent, all resetting of states required
            for a succesful export should be done here instead
        """

        self.output = None
        self.name = layer.name()
        self.aliasing = kwargs.get("aliasing", False)
        if exportname is not None:
            self.exportname = exportname
        else:
            self.exportname = self.name

        if path is None:
            path = save_location()
        self.savepath = path

        layout = self.layoutmanager.layoutByName(self.name)

        mapsize = self.settings["mapsize"]
        resolution = self.settings["map_reso"]
        tilesize = self.settings["tilesize"]
        tilesize_enabled = self.settings["tilesize_enabled"]

        utm_zone = extent_utm_zone()
        self.extent = transform_extent(utm_zone)
        self.mapsize = mapsize
        self.margin = 400
        self.tilesize = min(mapsize, self.TEMPSIZE)
        self.tilecount = math.ceil(mapsize / self.TEMPSIZE)
        self.updatePrintManager(layer, utm_zone)

    def handleTile(self, column: int = 0, row: int = 0):
        """
            Handles setting up the print manager for a row/column within the selected areaself.
            (0, 0) is top left coordinate (Default coordinates for both qgis layouts and QImage)
        """

        left = self.extent[0] + column * self.tilesize
        right = min(left + self.tilesize, self.extent[2]) + self.margin
        top = self.extent[3] - row * self.tilesize
        bottom = min(top - self.tilesize, self.extent[1]) - self.margin

        self.itemmap.setExtent(QgsRectangle(left, bottom, right, top))
        self.itemmap.attemptResize(QgsLayoutSize(right - left, top - bottom, QgsUnitTypes.LayoutPixels))
        name = f"gtt_{column}_{row}"
        filepath = (self.savepath / name).with_suffix(self.extension)
        message_log(f"Exporting {name}")
        self._task = self.saveImage(filepath)
        self.tiles.append((self._task.filepath, column, row))
        return self._task

    def get_tile(self, column=0, row=0) -> QImage:
        tile = self.savepath / f"gtt_{column}_{row}{self.extension}"
        image = QImage()
        image.load(str(tile))  # No error if file doesnt exist
        return image

    def mergeTiles(self) -> QImage:
        """
            Loads tiles and merges them with a QPainter device
            This also handles tiling, where it loads only required Temptiles and saves those
        
        Returns:
            QImage: Loaded image
        """
        message_log("Merging tiles")

        mapsize = self.settings["mapsize"]
        if self.settings["tilesize_enabled"]:
            tilesize = self.settings["tilesize"]
            tiles = math.ceil(mapsize / tilesize)
        else:
            tiles = 1
            tilesize = mapsize

        for column in range(0, tiles):
            for row in range(0, tiles):

                width = min(tilesize, mapsize - (column * tilesize))
                height = min(tilesize, mapsize - (row * tilesize))
                rect = (column * tilesize, row * tilesize, width, height)

                start_x = math.floor(rect[0] / self.TEMPSIZE)
                start_y = math.floor(rect[1] / self.TEMPSIZE)
                amount_x = math.ceil(width / self.TEMPSIZE)
                amount_y = math.ceil(height / self.TEMPSIZE)

                combined = QImage(tilesize + self.TEMPSIZE, tilesize + self.TEMPSIZE, QImage.Format_RGB32,)
                painter = QPainter(combined)

                for tile_row in range(0, amount_y):
                    for tile_column in range(0, amount_x):
                        image = self.get_tile(tile_column + start_x, tile_row + start_y)

                        position = QPoint(tile_column * self.TEMPSIZE, tile_row * self.TEMPSIZE)
                        painter.drawImage(position, image)
                        del image

                ## Crop the image to the actual dimensions
                offset_x = (column * tilesize) % self.TEMPSIZE
                offset_y = (row * tilesize) % self.TEMPSIZE
                rectangle = (offset_x, offset_y, width, height)

                painter.end()
                if tiles == 1:
                    file_obj = save_location() / f"{self.exportname}{self.extension}"
                else:
                    file_obj = save_location() / f"{self.exportname}_{column}_{row}{self.extension}"

                filepath = str(file_obj)
                crop: QImage = combined.copy(QRect(*rectangle))
                del combined

                if self.settings["scaled"]:
                    scaling = self.settings["scaled_size"] / mapsize
                    crop = crop.scaled(
                        int(crop.width() * scaling), 
                        int(crop.height() * scaling), 
                        Qt.IgnoreAspectRatio,
                        Qt.SmoothTransformation,
                    )

                if self.settings["map_reso"] != -1:
                    scaling = self.settings["map_reso"] / mapsize
                    crop = crop.scaled(
                        int(crop.width() * scaling), 
                        int(crop.height() * scaling), 
                        Qt.IgnoreAspectRatio,
                        Qt.SmoothTransformation,
                    )

                message_log(f"Exporting to {file_obj.name}")
                crop.save(filepath)

        ## Cleanup the temporary tiles
        if self.settings["tile_cleanup"]:
            for tile, _, _ in self.tiles:
                try:
                    os.remove(tile)
                except Exception:
                    pass  # Ignore this, not that important if file is in use

        return filepath

    def updatePrintManager(self, layer, utm):
        """Resizes a print manager to given size"""

        # Workaround for QgsLayoutItemMap turning into QgsLayoutItem after restart,
        # in which cas we can't set extent and control layers
        layout = self.layoutmanager.layoutByName(self.name)
        if layout is None:
            layout = self.createPrintManager()

        itemMap = layout.itemById(self.name)
        if itemMap is None or "QgsLayoutItemMap " not in str(itemMap):
            self.layoutmanager.removeLayout(layout)
            layout = self.createPrintManager()

        self.layout = layout
        page = layout.pageCollection().pages()[0]
        page.setPageSize(QgsLayoutSize(self.tilesize, self.tilesize, QgsUnitTypes.LayoutPixels))
        itemMap = self.itemmap = layout.itemById(self.name)
        itemMap.attemptResize(QgsLayoutSize(self.tilesize, self.tilesize, QgsUnitTypes.LayoutPixels))
        itemMap.setCrs(QgsCoordinateReferenceSystem(utm))
        itemMap.setLayers([layer])
        print(f"Tilesize: {self.tilesize}, CRS: {utm}")

    def createPrintManager(self):
        """Creates a QGIS Print manager with the proper settings"""

        # get a reference to the layout manager
        project = QgsProject.instance()
        manager = project.layoutManager()
        layout = QgsPrintLayout(project)

        layout.initializeDefaults()
        layout.setName(self.name)
        layout.setUnits(QgsUnitTypes.LayoutPixels)

        manager.addLayout(layout)
        # create a map item to add

        itemMap = QgsLayoutItemMap(layout)
        itemMap.setId(self.name)
        layout.addLayoutItem(itemMap)
        return layout

    def saveImage(self, filepath: Path):
        self._task = SaveImage(filepath, self.layout, aliasing=self.aliasing)
        self._task.error.connect(self.save_error)
        QgsApplication.taskManager().addTask(self._task)
        return self._task

    def save_error(self, errcode, errstring):
        iface.messageBar().pushCritical("Printer ERROR", f"{errcode} - {errstring}")
        if errcode == 3:
            message_log(f"{errstring} - Full disk?", level=Qgis.Critical)
        else:
            message_log(f"{errstring}", level=Qgis.Critical)


class SaveImage(QgsTask):
    filepath: Path
    error = pyqtSignal(int, str)

    def __init__(self, filepath: Path, layout, aliasing=False, **kwargs):
        name = filepath.name
        super().__init__(description=f"Exporting image '{name}'")

        self.layout = layout
        self.result = None
        self.filepath = filepath
        self.aliasing = aliasing

    def run(self):
        """Saves given layout as an image"""

        print(f"Saving {self.filepath}")
        self.filepath.parent.mkdir(parents=True, exist_ok=True)
        layout = self.layout
        exporter = QgsLayoutExporter(layout)
        context = QgsLayoutRenderContext(layout)
        context.setFlag(context.FlagAntialiasing, self.aliasing)

        export_settings = exporter.ImageExportSettings()
        export_settings.generateWorldFile = False
        export_settings.dpi = 75
        export_settings.flags = context.flags()
        result = exporter.exportToImage(str(self.filepath), export_settings)

        iface._exporter = exporter
        iface._settings = export_settings

        layout.exporter = exporter
        layout.context = context
        layout.settings = export_settings
        self.result = result

        if result == exporter.Success:
            # If this file exists, arma's terrain builder won't import (LOL BIS!)
            filepathmeta = self.filepath.with_suffix(".bmp.aux.xml")
            if filepathmeta.exists():
                filepathmeta.unlink()
            return True

        elif result == exporter.Canceled:
            self.error = result
            errstring = "Canceled"
        elif result == exporter.FileError:
            self.ferror = result
            errstring = "FileError"
        elif result == exporter.IteratorError:
            self.iteraerror = result
            errstring = "IteratorError"
        elif result == exporter.MemoryError:
            self.memerror = result
            errstring = "MemoryError"
        elif result == exporter.PrintError:
            self.prerror = result
            errstring = "PrintError"
        elif result == exporter.SvgLayerError:
            self.svglaerror = result
            errstring = "SvgLayerError"
        else:
            errstring = "Unknown error"

        self.error.emit(result, errstring)
        return False
