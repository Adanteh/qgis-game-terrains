import threading

from PyQt5.QtCore import QObject, pyqtSignal

from qgis import core
from qgis.utils import iface
from qgis.core import QgsSettings, QgsExpressionContextUtils

from gtt.static import SETTINGSNAME, PROJECT_URL
from gtt.markarea import MarkAreaTool
from gtt.functions import message_log
from gtt.extent import get_working_layer

from gtt.functionsmisc import reset_settings, clear_cache, pydevd_attach
from gtt.ui.functions import add_menu_action
from gtt.exporter import GttExport
from gtt.engine.base import Engine
from gtt.install import update_available

from typing import Dict

class GttTools(QObject):
    """Container for general business logic"""
    setting_changed = pyqtSignal(str, object)
    version_check_done = pyqtSignal(bool)
    engine: Engine
    settings: Dict

    def __init__(self, parent=iface):
        super().__init__(parent=parent)

        self.actions = []
        self.loadSettings()
        self.engine = Engine()
        iface.gtt = self

        self.context = core.QgsProcessingContext()
        self.context.setProject(core.QgsProject.instance())
        self.feedback = core.QgsProcessingFeedback()
        self.addModules()
        self.loadActions()

        self.version_check_done.connect(self._versionNotify)
        thread = threading.Thread(target=self.versionCheck)
        thread.start()

    def versionCheck(self, manual=False):
        if update_available():
            self.version_check_done.emit(True)
        else:
            if manual:
                self.version_check_done.emit(False)

    def _versionNotify(self, update):
        if update:
            iface.messageBar().pushMessage(
                f"A Game Terrain Tools update is available on <a href='{PROJECT_URL}'>Gitlab</a>", duration=20,
            )
            message_log("A Game Terrain Tools update is available on gitlab")
        else:
            iface.messageBar().pushSuccess("GTT", "Latest version of GTT is installed")
            message_log("Latest version of Game Terrain Tools already installed")

    def addModules(self):
        self.exporter = GttExport(self, parent=self)
        self.markarea = MarkAreaTool(iface.mapCanvas(), self)
        self.markarea.marked.connect(self.set_settings_to_extent)

    def loadActions(self):
        self.actions.append(add_menu_action(reset_settings, "Reset settings"))
        self.actions.append(add_menu_action(clear_cache, "Clear download cache"))
        self.actions.append(add_menu_action(pydevd_attach, "Debug attaching"))
        self.actions.append(add_menu_action(lambda: self.versionCheck(True), "Check for updates"))

    def unload(self):
        for action in self.actions:
            iface.removePluginMenu("Game Terrain Tools", action)

    def setProgress(self, value: int):
        widget = iface.gtt_plugin.widget("primary")
        if widget is not None:
            widget.export_progressbar.setValue(value)

    # --------------------------------------------------------------------------
    def loadSettings(self):
        """
            Loads all the QSettings used for this plugin.
            Register non-set settings, so we can configure them in QGIS preferences
        """

        self.settings = {}
        settings_registered = {
            "engine": "Arma",
            "widget_open": True,
            "primary_tabs": 0,
            "mapsize": 4096,
            "tilesize": 4096,
            "tile_cleanup": True,
            "satmap": "bing",
            "zoom_level": 17,
            "scaled": False,
            "scaled_size": 4096,
            "map_reso": -1,
            "tilesize_enabled": False,
            "export_satmap": True,
            "export_mask": True,
            "roads_raw": False,
            "mask_type": "osm",
            "otb/memory": 2048,
            "otb/location": "",
            "export_roads": True,
            "export_roads_raster": True,
            "export_shapes": False,
            "export_height": True,
            "scaled_height": True,
            "lower_to_zero": False,
            "height_reso": 4096,
            "heightmap": "srtm",
            "advanced": 0,
        }

        self._settings = settings = QgsSettings()

        print(f"Restoring settings '{SETTINGSNAME}'")
        for key, default in settings_registered.items():

            # We do dont give a type initially, so we can check if a setting exists,
            # instead of retrieving a value of the default type (0 for int for example,
            # which might be the actual existing setting)
            setting = settings.value(f"{SETTINGSNAME}/{key}", None)
            if setting is not None:
                setting = settings.value(f"{SETTINGSNAME}/{key}", None, type=type(default))
            else:
                settings.setValue(f"{SETTINGSNAME}/{key}", default)
                setting = default

            print(f"'{key}' = {setting}  [{default}]")
            self.settings[key] = setting

    def setting_set(self, setting: str, value):
        self.settings[setting] = value
        self._settings.setValue(f"{SETTINGSNAME}/{setting}", value)
        self.setting_changed.emit(setting, value)
        return value

    def settings_extent_map(self) -> tuple:
        """
        Gets a map of settings that we want to set/get from our extent layer.
        Note these are actually project settings

        Returns:
            Tuple: All setting names we want to save/load from extent
        """
        return (
            "engine",
            "mapsize",
            "tilesize",
            "satmap",
            "zoom_level",
            "scaled",
            "scaled_size",
            "map_reso",
            "tilesize_enabled",
            "roads_raw",
            "mask_type",
            "scaled_height",
            "height_reso",
            "heightmap",
        )

    def set_settings_to_extent(self, *args, **kwargs):
        """
            This sets properties for settings we want to save. Allows for quickly reloading old settings from a project
            but still keeping 'global' settings as a primary mechanic
        """
        layer = get_working_layer()
        for name in self.settings_extent_map():
            QgsExpressionContextUtils.setLayerVariable(layer, name, str(self.settings[name]))

    def set_settings_from_extent(self):
        """
            Loads settings from the extent layer (If they have) and rewrite them to our expected types.
            This allows user to have general settings, but still load from existing projects without too much hassle
        """
        layer = get_working_layer()
        if layer is None:
            iface.messageBar().pushCritical("GTT", "No working layer to load settings from")
            return

        layer_scope = QgsExpressionContextUtils.layerScope(layer)
        print("Loading settings from extent")
        for name in self.settings_extent_map():
            value = layer_scope.variable(name)
            if value is not None:  # Skip settings not present in the extent layer
                current = self.settings[name]
                try:
                    if isinstance(current, bool):  # Rewrite setting
                        value = True if "True" else False
                    elif isinstance(current, int):
                        value = int(value)
                    elif isinstance(current, float):
                        value = float(value)
                    self.setting_set(name, value)
                    print(f"'{name}' = {value}")
                except ValueError:
                    pass

    def mark_area(self):
        self.markarea.draw_square()

    def export(self, types):
        message_log("<b>STARTING EXPORT</b>")
        self.exporter.export(types=types)
