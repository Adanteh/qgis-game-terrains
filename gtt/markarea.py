import shutil
from PyQt5.QtGui import QColor, QCursor, QPixmap
from PyQt5.QtCore import pyqtSignal
from qgis import core
from qgis.gui import QgsMapTool, QgsRubberBand
from qgis.core import QgsWkbTypes
from qgis.utils import iface

from gtt.extent import EXTENT, get_working_layer, transform_extent, wgs_to_utm
from gtt.functions import create_transform, move_layer_to_top, FOLDER, project_folder


def create_cursor():
    return QCursor(
        QPixmap(
            [
                "16 16 3 1",
                "      c None",
                ".     c #FF0000",
                "+     c #800080",
                "                ",
                "       +.+      ",
                "      ++.++     ",
                "     +.....+    ",
                "    +.  .  .+   ",
                "   +.   .   .+  ",
                "  +.    .    .+ ",
                " ++.    .    .++",
                " ... ...+... ...",
                " ++.    .    .++",
                "  +.    .    .+ ",
                "   +.   .   .+  ",
                "   ++.  .  .+   ",
                "    ++.....+    ",
                "      ++.++     ",
                "       +.+      ",
            ]
        )
    )


class MarkAreaTool(QgsMapTool):
    marked = pyqtSignal(bool)
    rubberband: QgsRubberBand

    def __init__(self, canvas, tools):
        super().__init__(canvas)
        self.canvas = canvas
        self.tools = tools
        self.rubberband = None

        self.xc = None
        self.yc = None
        self.moving = False
        self.cursor = create_cursor()

        self.transform_wgs84 = None
        self.transform_utm = None
        self.utm_id = None

    def activate(self):
        self.canvas.setCursor(self.cursor)
        self.create_transforms()
        self.create_rubberband()
        self.moving = True

    def create_transforms(self):
        project_crs = self.canvas.mapSettings().destinationCrs().authid()
        self.transform_wgs84 = create_transform(project_crs, "EPSG:4326")
        self.project_crs = project_crs

    def get_utm_transform(self, xc, yc, to=False):
        utm_id = self.utm_id = wgs_to_utm(self.yc_wgs84, self.xc_wgs84, authid=True)
        return self.create_transform_utm(utm_id, to=to)

    def create_transform_utm(self, utm_id, to=False):
        """This will transform to the UTM zone the mouse is currently in on the fly"""
        if to:
            return create_transform(self.project_crs, utm_id)
        else:
            return create_transform(utm_id, self.project_crs)

    def create_rubberband(self):
        color = QColor(255, 0, 0, 255)
        self.rubberband = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.rubberband.setColor(color)
        self.rubberband.setFillColor(QColor(0, 0, 0, 100))
        self.rubberband.setWidth(1)
        return self.rubberband

    def offset_coordinates(self, offset, xc, yc):
        transform = self.get_utm_transform(xc, yc, True)
        point = core.QgsPoint(xc, yc)
        point.transform(transform)
        xc = int(point.x())
        yc = int(point.y())

        left = xc - offset
        bottom = yc - offset
        top = xc + offset
        right = yc + offset
        return (left, bottom, top, right)

    def canvasPressEvent(self, event):
        pass

    def canvasMoveEvent(self, event):
        if not self.rubberband:
            return
        if not self.moving:
            return

        layer = get_working_layer()
        point = self.toLayerCoordinates(layer, event.pos())
        pointMap = self.toMapCoordinates(layer, point)

        self.xc_layer = pointMap.x()
        self.yc_layer = pointMap.y()

        self.xc = pointMap.x()
        self.yc = pointMap.y()

        point = core.QgsPoint(self.xc, self.yc)
        point.transform(self.transform_wgs84)
        self.xc_wgs84 = point.x()
        self.yc_wgs84 = point.y()

        # Convert to the proper UTM coordinates, for accurate offset in meters
        offset = self.tools.settings["mapsize"] / 2
        points = self.points = self.offset_coordinates(offset, self.xc, self.yc)
        rectangle = core.QgsRectangle(*points)
        geom = core.QgsGeometry.fromRect(rectangle)

        # Convert back to the project_crs, so we can show it on the rubberband
        geom.transform(create_transform(self.utm_id, self.project_crs))
        self.rubberband.reset(QgsWkbTypes.PolygonGeometry)
        self.rubberband.setToGeometry(geom, None)

    def canvasReleaseEvent(self, event):
        if not self.moving:
            self.moving = True
        else:
            self.rubberband.reset(QgsWkbTypes.PolygonGeometry)
            self.moving = False
            self.mark_feature()
            self.canvas.unsetMapTool(self)
        if self.rubberband:
            return

    def showSettingsWarning(self):
        pass

    def deactivate(self):
        self.rubberband.reset(QgsWkbTypes.PolygonGeometry)
        self.rubberband = None
        self.canvas.refresh()
        self.moving = False

    def isZoomTool(self):
        return False

    def isTransient(self):
        return False

    def isEditTool(self):
        return True

    def draw_square(self):
        if str(project_folder()) == ".":
            iface.messageBar().pushCritical("Error", "Please save the QGIS Project before marking area")
            return False

        self.canvas.setMapTool(self)
        return True

    def create_scratchlayer(self):
        """Creates a scratch layer if one doesn't exist yet for this tool"""

        layer = get_working_layer()
        if layer is None:
            output_file = FOLDER / "data" / "scratch_layer.gpkg"
            style_file = FOLDER / "data" / "map_extent.qml"
            target_file = project_folder() / "scratch_layer.gpkg"
            shutil.copy(output_file, target_file)

            layer = core.QgsVectorLayer(str(target_file), EXTENT, "ogr")

            core.QgsProject.instance().addMapLayer(layer)
            layer.loadNamedStyle(str(style_file))
            layer.updateFields()

        return layer

    def mark_feature(self):
        """Creates the feature (Polygon showing the corners of square we created"""
        layer = self.create_scratchlayer()

        layer.beginEditCommand("Terrain area draw")
        layer.setCrs(core.QgsCoordinateReferenceSystem(self.utm_id))
        layer.startEditing()
        layer.deleteFeatures([feat.id() for feat in layer.getFeatures()])

        feature = core.QgsFeature()
        feature.setGeometry(core.QgsGeometry.fromRect(core.QgsRectangle(*self.points)))

        # add attribute fields to feature
        provider = layer.dataProvider()

        provider.addFeature(feature)
        layer.commitChanges()
        layer.endEditCommand()
        layer.updateExtents()

        # Set visibility to true again
        core.QgsProject.instance().layerTreeRoot().findLayer(layer.id()).setItemVisibilityChecked(True)
        move_layer_to_top(layer)
        self.canvas.refresh()

        extent = layer.extent()
        left, bottom, right, top = [
            extent.xMinimum(),
            extent.yMinimum(),
            extent.xMaximum(),
            extent.yMaximum(),
        ]
        print(f"'{layer.crs().authid()}' {right - left}, {top - bottom}")

        extent = transform_extent(self.utm_id)
        left, bottom, right, top = extent

        self.marked.emit(True)

        return feature
