# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OSMRequest
                                 A QGIS plugin
 Plugin to download OSM data by area
                             -------------------
        begin                : 2015-04-07
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Brazilian Army - Geographic Service Bureau
        email                : suporte.dsgtools@dsg.eb.mil.br
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import urllib.error
import urllib.parse
import urllib.request
from pathlib import Path
from PyQt5.QtCore import QObject, QSettings, pyqtSignal, pyqtSlot

from qgis.core import Qgis, QgsApplication, QgsTask
from qgis.utils import iface
from gtt.functions import show_progress, message_log, get_temp_path
from gtt.functions import get_uid


class UrllibDownloader(QgsTask):
    proxyOpened = pyqtSignal(str)
    progressReport = pyqtSignal(float, float)

    def __init__(self, filename):
        super().__init__(description="Downloading OSM data")

        self.filename = filename
        self.exception = None
        self.total_size = 1
        self.current_size = 0
        self.error = None
        self.error_data = None

    def getProxyConfiguration(self):
        settings = QSettings("QGIS", "QGIS3")
        settings.beginGroup("proxy")
        enabled = str(settings.value("proxyEnable"))
        host = str(settings.value("proxyHost"))
        port = str(settings.value("proxyPort"))
        user = str(settings.value("proxyUser"))
        password = str(settings.value("proxyPassword"))
        type = str(settings.value("proxyType"))
        settings.endGroup()
        return (enabled, host, port, user, password, type)

    def setUrllibProxy(self):
        (enabled, host, port, user, password, type) = self.getProxyConfiguration()
        if enabled == "false" or type != "HttpProxy":
            return

        proxyStr = "http://" + user + ":" + password + "@" + host + ":" + port
        self.proxyOpened.emit(host + "| Port: " + port)

        proxy = urllib.request.ProxyHandler({"http": proxyStr})
        opener = urllib.request.build_opener(proxy, urllib.request.HTTPHandler)
        urllib.request.install_opener(opener)

    def set_request(self, url, xml):
        req = urllib.request.Request(url=url, data=xml, headers={"Content-Type": "application/xml"})
        self.request = req
        return req

    def run(self):
        self.setUrllibProxy()

        try:
            response = urllib.request.urlopen(self.request)
        except urllib.error.URLError as e:
            self.error = f"Error occurred: {e.args} Reason: {e.reason}"
            self.error_data = e
            return False
        except urllib.error.HTTPError as e:
            self.error = f"Error occurred: {e.code} Reason: {e.msg}"
            self.error_data = e
            return False

        Path(self.filename).parent.mkdir(mode=0o777, parents=True, exist_ok=True)
        local_file = open(self.filename, "wb")

        meta = response.info()
        self.total_size = meta.get("Content-Length", -1)  # Size in bytes
        self.current_size = 0  # Size in MB
        iface.meta = meta

        block_size = 1024 * 8
        while not self.isCanceled():
            buffer = response.read(block_size)
            if not buffer:
                break

            try:
                local_file.write(buffer)
                len_ = len(buffer)
                size = len_ / float(1000000)
                self.current_size += size
                self._update_progress()

            except Exception:
                local_file.close()
                self.error = "An error occurred writing the osm file."
                return False

        local_file.close()
        if self.isCanceled():
            response.close()
            return False
        else:
            self.error = None
            return True

    def _update_progress(self):
        if self.total_size != -1:  # Some services like OSM api don't give us a size
            percentage = self.current_size * 1000000 / self.total_size * 100
            self.setProgress(percentage)
        else:
            percentage = 0
        self.progressReport.emit(round(self.current_size, 2), percentage)

    # QgsTask implementations
    # --------------------------------------------------------------------------
    # def description():
    #     return "Downloading OpenStreetmap data"


class OsmDownloader(QObject):
    done = pyqtSignal(str)
    progressChanged = pyqtSignal(float)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.filepath = ""
        self.progress = None

    def download(self, bounds):
        """Uses the OSM downloader to grab data for the selected area"""

        # Initiating processing
        self.filepath = get_temp_path() / "osmtemp_{}.osm".format(get_uid())
        self.progress = show_progress("Starting OSM download")

        downloader = UrllibDownloader(self.filepath)
        xmlfile = self.make_post_file(self.create_base_xml(), *bounds)

        # Connecting end signal
        downloader.begun.connect(self.begun)
        downloader.proxyOpened.connect(self.proxy_opened)
        downloader.progressReport.connect(self.progress_report)
        downloader.taskCompleted.connect(self.task_completed)
        downloader.taskTerminated.connect(self.task_terminated)

        self.downloader = downloader
        self.downloader.set_request("http://overpass-api.de/api/interpreter", xmlfile)
        QgsApplication.taskManager().addTask(downloader)

    def create_base_xml(self):
        xml_data = '<osm-script timeout="120">'
        xml_data += '<union into="_">'
        xml_data += '<bbox-query e="maxlong" n="maxlat" s="minlat" w="minlong"/>'
        xml_data += '<recurse type="up"/><recurse type="down"/>'
        xml_data += '</union><print limit="" mode="meta" order="id"/>'
        xml_data += "</osm-script>"
        return xml_data

    def make_post_file(self, xml_data, minLong, minLat, maxLong, maxLat):
        xml_data = xml_data.replace("maxlong", str(maxLong))
        xml_data = xml_data.replace("maxlat", str(maxLat))
        xml_data = xml_data.replace("minlong", str(minLong))
        xml_data = xml_data.replace("minlat", str(minLat))
        xml_data = xml_data.encode("utf-8")
        return xml_data

    @pyqtSlot(str)
    def proxy_opened(self, text):
        message_log(f"Proxy opened: {text}")

    @pyqtSlot()
    def begun(self):
        self.progress.setText("Connecting to OSM servers...")

    @pyqtSlot(float)
    def progress_report(self, filesize, progress=0):
        self.progress.setText("Downloaded: " + "{0:.2f}".format(filesize) + " Mb from OSM servers...")
        # self.progress.setProgress(progress)  # No known filesize for OSM API

    @pyqtSlot()
    def task_completed(self):
        if self.downloader.error is not None:
            self.task_terminated()
            return

        self.progress.setRange(0, 1)
        self.progress.setProgress(1)

        message_log("Downloaded: " + "{0:.2f}".format(self.downloader.current_size) + " Mb from OSM servers...")
        self.progress.setText("Finished downloading")
        self.done.emit(str(self.filepath))

    @pyqtSlot()
    def task_terminated(self):
        if self.downloader.error is not None:
            self.progress.setText(self.downloader.error)
            message_log(self.downloader.error, level=Qgis.Critical)
        else:
            self.progress.setText("Failed to download from OSM servers...")
        self.progress.setLevel(Qgis.Critical)

        # Clearer error handling
        if self.downloader.error_data is not None:
            error = self.downloader.error_data
            if error.msg == "Too Many Requests":
                message_log(
                    "Limited amount of downloads allowed, wait at least 5 minutes and try again", level=Qgis.Critical,
                )
