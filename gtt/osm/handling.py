from functools import partial
from pathlib import Path
from PyQt5.QtCore import pyqtSignal, QObject
from qgis.core import QgsProject, QgsVectorLayer, QgsApplication, QgsProcessingAlgRunnerTask
from qgis.utils import iface

from gtt.functions import get_layer, show_progress, get_uid, project_folder
from gtt.extent import get_working_layer

from . import roads as osm_roads
from . import shapes as osm_shapes


class OsmHandling(QObject):
    done = pyqtSignal()
    progressChanged = pyqtSignal(float)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.progress = None
        self.task_clip = None
        self.task = None
        self.types = ["lines", "multipolygons"]

    def run(self, filepath):
        self.filepath = filepath.split("|")[0]
        self.progress = show_progress("Processing OSM data", range_=(0, 100))
        self.count = 0

        layer = get_layer("osm roads")
        if layer is not None:
            QgsProject.instance().removeMapLayers([layer.id()])

        layer = get_layer("osm shapes")
        if layer is not None:
            QgsProject.instance().removeMapLayers([layer.id()])

        self.import_osm_layers()

    def import_osm_layers(self):
        """Imports the OSM layers"""
        # Some notes, for some reason bot setSubsetString recreates non-existant layers magically.
        # Just keep all layers and delete all Clipped layers at the end, seems the only thing that works
        # Also use setName at the end, because using it anywhere else fucks shit up :D

        if self.count >= len(self.types):
            self._finish()
            return True

        count = self.count
        layertype = self.types[count]
        self.count += 1

        layername = Path(self.filepath).stem + f"_{layertype}"
        sourcelayer = QgsVectorLayer(self.filepath + "|layername=" + layertype, layername, "ogr")
        if sourcelayer is not None:
            QgsProject.instance().addMapLayer(sourcelayer)
            self.clip_layer(sourcelayer)

    def clip_layer(self, sourcelayer):
        """Clips given source layer by our map area"""

        layertype = sourcelayer.source().split("|")[1].replace("layername=", "")
        layername = f"osm {layertype}"

        extent = get_working_layer()
        uid = get_uid()
        target_path = project_folder()
        temp_file = str(target_path / f"{layername}.shp")

        if isinstance(sourcelayer, str):
            sourcelayer = get_layer(sourcelayer)

        parameters = {
            "INPUT": sourcelayer,
            "OVERLAY": extent.source(),
            "OUTPUT": temp_file,
        }

        alg = "native:clip"
        alg = QgsApplication.processingRegistry().algorithmById(alg)
        context = iface.gtt.context
        feedback = iface.gtt.feedback
        self.task_clip = QgsProcessingAlgRunnerTask(alg, parameters, context, feedback)
        self.task_clip.executed.connect(partial(self.clip_layer_done, parameters))
        QgsApplication.taskManager().addTask(self.task_clip)

    def clip_layer_done(self, params):
        output = params["OUTPUT"]
        sourcelayer = params["INPUT"]
        layertype = sourcelayer.source().split("|")[1].replace("layername=", "")

        clippedlayer = QgsVectorLayer(output, "Clipped", "ogr")
        QgsProject.instance().removeMapLayers([sourcelayer.id()])
        # QgsProject.instance().addMapLayer(clippedlayer)

        if clippedlayer is not None:
            clippedlayer.setName("osm {}".format(layertype))

            self.filter_layers(clippedlayer, layertype)
            if layertype == "lines":
                self.progress.setText("Processing roads")
                self.task = osm_roads.ProcessRoads(clippedlayer)
                self.task.taskCompleted.connect(self._continue)
                self.task.progressChanged.connect(self.progress.setProgress)
                self.task.progressChanged.connect(self.progressChanged.emit)
                QgsApplication.taskManager().addTask(self.task)

            elif layertype == "multipolygons":
                self.progress.setText("Processing shapefiles")
                self.task = osm_shapes.ProcessShapes(clippedlayer)
                self.task.taskCompleted.connect(self._continue)
                self.task.progressChanged.connect(self.progress.setProgress)
                self.task.progressChanged.connect(self.progressChanged.emit)
                QgsApplication.taskManager().addTask(self.task)

        else:
            self.import_osm_layers()

    def _continue(self):
        if self.task is not None:
            print(self.task, self.task.layer)
            QgsProject.instance().addMapLayer(self.task.layer)
            # self.task = None
        if self.progress is not None:
            self.progress.setDuration(5)
        self.import_osm_layers()

    def _finish(self):
        """Make sure the Clipped layers actually stay away (Sometimes they get readded magically)"""
        # layers = [QgsProject.instance().removeMapLayers([layer.id()]) for layer in QgsProject.instance().mapLayersByName('Clipped')]
        iface.mapCanvas().refresh()
        self.done.emit()

    def filter_layers(self, layer, layertype):
        """Applies filter for layer"""

        layerfilter = ""
        if layertype == "lines":
            layerfilter = "\"highway\" != 'NULL'"
        if layertype == "multipolygons":
            layerfilter = "\"landuse\" in ('forest') OR \"natural\" in ('wood')"  # meadow landuse
            layerfilter = ""  # meadow landuse

        if layerfilter != "":
            layer.setSubsetString(layerfilter)
        return 0
