import importlib.util
import subprocess
import configparser
import re
import requests
import importlib
from pathlib import Path

from qgis.core import QgsSettings
from gtt.functions import pyexe
from gtt.static import SETTINGSNAME, PROJECT_ID


def install_file(path: Path, force=False):
    """Install requirements file on location `path`"""
    print("Installing requirements")
    cmd = [
        str(pyexe()),
        "-m",
        "pip",
        "install",
        "-r",
        str(path),
        "--user",
    ]

    if force:
        cmd.append("--force")
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(f"Return code: {result.returncode}")
    print(f"stderr: {result.stderr}")
    print(f"stdout: {result.stdout.decode('utf-8')}")


def import_test_fail() -> bool:
    """Attempts to find all modules listed on separate line of requirements_test.txt
    
    Returns:
        bool: True if one or more modules not found, False if all found
    """
    try:
        with (Path(__file__).parent / "requirements.txt").open(mode="r") as fp:
            line = fp.readline().strip("\n").split("=")[0]
            importlib.util.find_spec(line)
    except ImportError:
        return True
    return False


def version_config() -> configparser.ConfigParser:
    metadata = Path(__file__).parent / "metadata.txt"
    config = configparser.ConfigParser()
    config.read(metadata)
    return config


def get_version() -> str:
    """Gets the current version from metadata"""
    config = version_config()
    version = config["general"]["version"]
    if version.startswith("v"):
        version = version[1:]
    return str(version)


def get_version_remote() -> str:
    """Gets the latest release number of gitlab wiki page"""
    # curl --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.example.com/api/v4/projects/1/wikis/home
    try:
        url = f"https://gitlab.com/api/v4/projects/{PROJECT_ID}/wikis/"
        with requests.session() as session:
            response = session.get(url + "version")
        return response.json()["content"]
    except Exception:
        return None


def version_compare(version1, version2):
    """Compares version numbers, -1 if version1 is older, 0 if equal, 1 if version1 is newer"""
    if version1.startswith("v"):
        version1 = version1[1:]

    if version2.startswith("v"):
        version2 = version2[1:]

    try:

        def normalize(v):
            return [int(x) for x in re.sub(r"(\.0+)*$", "", v).split(".")]

        a = normalize(version1)
        b = normalize(version2)
        return (a > b) - (a < b)
    except ValueError:
        return 0


def update_available() -> bool:
    """Compares local version to latest version available on gitlab, returns True if newer version exists
    
    Returns:
        bool: True if remote version is newer
    """

    local = get_version()
    remote = get_version_remote()
    if remote is not None:
        if version_compare(local, remote) < 0:
            return True
    return False
