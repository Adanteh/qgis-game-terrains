import json
import subprocess
import threading
from pathlib import Path
from typing import Tuple

from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtGui import QColor

from gtt.extent import extent_utm_zone, transform_extent
from gtt.functions import (
    get_layer,
    save_location,
    message_log,
    qgis_bin_folder,
    get_uid,
    get_engine
)
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsProject,
    QgsRasterLayer,
)
from qgis.utils import iface

from . import install, ui
from .roi import functions, surfaces
from .roi.symbol import create_banded_colors
from .run import run
from .save import save as save_rendered

ROI_NAME = "gtt_roi"
WIKI_LINK = r"https://gitlab.com/Adanteh/qgis-game-terrains/wikis/Advanced-Mask"


def export(exportname: str):
    """Base export function, handles all checks required to be able to export a classified mask
    Args:
        name (str): [description]
    """

    if not precheck():
        return 1

    # Determine which sort of source we have, either a clean gtt_satmap,
    # or a imported file geolocated to the Arma mapframe
    mode = "bitmap"
    layer = get_layer("gtt_satmap")
    if layer is not None:
        satmap = Path(layer.source())
        try:
            exists = satmap.exists()
            if exists:
                if satmap.suffix == ".tif":
                    mode = "imported"
        except OSError:  # not a valid path
            pass

    if mode == "bitmap":
        satmap = save_location() / "gtt_satmap.bmp"
        if not satmap.exists():
            iface.messageBar().pushCritical(
                "Missing satmap", f"You need to have an exported 'gtt_satmap.bmp' to export classified mask",
            )
            message_log(
                f"Missing 'gtt_satmap.bmp' file, make sure you run Export Satmap first", level=Qgis.Critical,
            )
            return 2
        task = from_bmp(satmap)
    else:
        task = from_imported_tif(satmap)
    return task


def import_bmp(path: Path):
    """Imports a bmp and geolocates it to the default engine coordinates
    Args:
        path (Path): Path to image file
    """

    engine = get_engine()

    size = get_file_dimensions(path)
    extent = (engine.COORD_X, engine.COORD_Y, engine.COORD_X + size, size)
    output = tiff_convert(path, crs=engine.CRS, extent=extent)
    if output is not None:
        layer = QgsRasterLayer(str(output), "gtt_satmap")
        QgsProject.instance().addMapLayer(layer)

        text = "Imported satmap, you can now create some regions of interest"
        iface.messageBar().pushSuccess("IMPORT", text)
        message_log(text)

        text = (
            "<br/><b>Important Notes</b><br/>"
            "This option is for non-geolocated satmaps in an empty project. Your ROIs should be created"
            " at the fake location the file is imported to, not the real world location!<br/>"
            "Make sure to check <b>only</b> Export Mask<br/>"
        )
        message_log(text)


def from_imported_tif(path: Path):
    """Runs classification from an imported Tif satmap
    Args:
        path (Path): path to the satmap file
    """

    classifier = Classification(parent=iface.gtt)
    task = classifier.main(path)
    task.done.connect(add_classified_file)
    return task


def from_bmp(path: Path):
    """Runs classification from an exporter BMP satmap
    Args:
        path (Path): Path to the exported satmap
    """
    if not precheck():
        return None

    crs = extent_utm_zone()
    output = tiff_convert(path, crs=crs)
    if output is not None:
        classifier = Classification(parent=iface.gtt)
        task = classifier.main(output)
        task.done.connect(add_classified_file)
        return task
    else:
        return None


def precheck():
    """Checks the requirements requried before we can run a classify algo"""

    layer_roi = get_layer(ROI_NAME)
    if layer_roi is None:
        iface.messageBar().pushCritical("Missing Layer", f"Layer '{ROI_NAME}' required for mask classification")
        message_log(
            f"Missing '{ROI_NAME}' layer, create one using the Mask wizard, or create one manually", level=Qgis.Warning,
        )
        return False
    else:
        if not list(layer_roi.getFeatures()):
            iface.messageBar().pushCritical("Missing features", f"Layer '{ROI_NAME}' has no features")
            message_log(
                f"Missing '{ROI_NAME}' features, create some using the Create Sample option", level=Qgis.Warning,
            )
            return False

    if not install.check_installed():
        iface.messageBar().pushCritical("OTB Install", "OTB toolbox not installed properly")
        message_log(
            "OrfeoToolbox install incorrect, please use the button to download, unpack and set the path to it",
            level=Qgis.Warning,
        )
        return False
    return True


def get_file_dimensions(path=r"B:\Arma\Frontline\QGIS_ArmaTerrainTools\Al-Thawrah\gtt_0_0.bmp",):
    """Uses GDAL to establish the dimensions of the file"""
    proc = subprocess.Popen([str(qgis_bin_folder() / "gdalinfo.exe"), str(path), "-json"], stdout=subprocess.PIPE,)
    out, err = proc.communicate()
    fileinfo = json.loads(out)
    return fileinfo["size"][0]


def tiff_convert(path: Path, crs="EPSG:32631", extent: Tuple[float] = None) -> Path:
    """Import this and save it as a georefended gtiff"""

    gdal_translate = qgis_bin_folder() / "gdal_translate.exe"
    output = path.parent / f"{path.stem}_projected.tif"

    if extent is None:
        extent = transform_extent(targetcrs=crs)

    left, bottom, right, top = extent
    cmd = [
        str(gdal_translate),
        "-of",
        "GTiff",
        "-a_srs",
        crs,
        str(path),
        str(output),
        "-a_ullr",
        f"{left}",
        f"{top}",
        f"{right}",
        f"{bottom}",
    ]
    print(cmd)

    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    if result.returncode == 0:
        return output
    else:
        message_log(
            "Failed converting to Tiff, check message log", level=Qgis.Critical, emit=True,
        )
        message_log(f"Command: {cmd}", level=Qgis.Critical, emit=False)
        message_log(f"Out: {result.stdout}", level=Qgis.Critical, emit=False)
        return None


class ThreadReturn(QObject):
    """Used to return back to main Qt event loop, from a python thread"""

    done = pyqtSignal(object, str)
    final = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.output = None
        self.error = None


class OtbException(Exception):
    pass


def add_classified_file(task: ThreadReturn, path: str):
    """Adds the classified tif file, and adds symbology based on classes for gray-band
    Args:
        task (ThreadReturn): QObject used to signal between threads
        path (str): path to the classified file
    """

    layer = QgsRasterLayer(path, "gtt_mask")
    QgsProject.instance().addMapLayer(layer)
    classes = surfaces.load_surfaces()
    create_banded_colors(layer, classes)
    output = save_classified_file(Path(path))
    task.output = output
    task.final.emit(task)


def save_classified_file(path: Path):
    """Changes grayband classified file to a properly rendered bitmap based on classes colors
    Args:
        path (Path): path to the classified file

    Returns:
        output (Path): path to the rendered bitmap
    """
    classes = surfaces.load_surfaces()
    colors = []
    for surface in classes["classes"].values():
        color = surface["color"]
        r, g, b, a = QColor(color).getRgb()
        colors.append((r, g, b))
    output = save_rendered(path, colors)
    return output


class Classification(QObject):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.add = False
        self.uid = 1
        self.progress = ui.Progress(parent=self)

    def main(self, path: Path) -> ThreadReturn:
        self.params = {"in": path}
        self.uid = get_uid()

        obj = ThreadReturn(parent=QgsApplication.instance())
        thread = threading.Thread(target=self.start, args=[obj])
        thread.start()
        return obj

    def test_thread(self, obj, step):

        # self.classify(self.params)
        self.params = self.handle_temp_name(self.params, "out")
        obj.done.emit(obj, self.params["out"])  # Add the result layer (Callback to main application)

    def start(self, obj):
        """Starts the full algorithm (Run in background thread)
        Args:
            obj (QObject): object to signal back to main thread
        """

        self.prepare()

        try:
            self.progress.set_progress_modifer(0.1)
            self.stats(self.params["in"])
            self.params = self.handle_temp_name(self.params, "out")
            self.progress.set_progress_modifer(0.4)
            self.training(Path(self.params["il"]))
            self.progress.set_progress_modifer(1)
            self.classify(self.params)
            self.params = self.handle_temp_name(self.params, "out")
            obj.done.emit(obj, self.params["out"])  # Add the result layer (Callback to main application)

        except OtbException:
            message_log("Classifiction error", level=Qgis.Critical)
            message_log("Please check LOG MESSAGES panel", level=Qgis.Critical)
            obj.final.emit(obj)
            self.progress.set_progress(0)

    def prepare(self):
        """Prepares our source material for classification process
        """
        layer = get_layer(ROI_NAME)
        classes = surfaces.load_surfaces()
        functions.sync_attributes(classes, layer)

    def handle_temp_name(self, params, key):
        """
            We save everything with UID per run, so we don't lose results when file gets locked.
            This will attempt to rename it to a name without the UID
        """
        current = Path(params[key])
        name = current.stem.split(".")[0]
        newpath = Path(current.parent / f"{name}{current.suffix}")

        try:
            current.replace(newpath)
            params[key] = str(newpath)
        except PermissionError:
            pass
        return params

    def stats(self, path):
        """Generate statistics file with OTB (Speeds up the other processes)"""

        folder = path.parent
        path_stats = folder / f"otb_stats.{self.uid}.xml"  # type: Path
        parameters = {"il": str(path), "out": str(path_stats)}

        folder = Path(iface.gtt.settings["otb/location"])
        self.params = parameters
        return_code = run(folder, "ComputeImagesStatistics", parameters, log=self.progress.log.emit)
        text = f"Finished 'ComputeImagesStatistics' with: {return_code}"
        message_log(text)

        if return_code != 0:
            raise OtbException

        return parameters

    def training(self, path):
        """Train the machine to not maximize paperclip production to heavily"""

        layer_roi = get_layer(ROI_NAME)
        path_roi = Path(layer_roi.source())

        folder = path.parent
        path_stats = folder / "otb_stats.xml"
        path_model = folder / "otb.model"
        path_matrix = folder / "otb_matrix.txt"

        parameters = {
            "io.il": str(path),
            "io.vd": str(path_roi),
            "io.imstat": str(path_stats),
            "io.out": str(path_model),
            "io.confmatout": str(path_matrix),
            "sample.vfn": "type",
            "classifier": "libsvm",
            "classifier.libsvm.k": "linear",
            "classifier.libsvm.m": "csvc",
            "classifier.libsvm.c": 1.0,
            "classifier.libsvm.nu": 0.5,
            "classifier.libsvm.opt": False,
            "classifier.libsvm.prob": False,
            "cleanup": True,
            "sample.mt": 1000,
            "sample.mv": 1000,
            "sample.bm": 1,
            "sample.vtr": 0.5,
        }

        folder = Path(iface.gtt.settings["otb/location"])
        self.params = parameters
        return_code = run(folder, "TrainImagesClassifier", parameters, log=self.progress.log.emit)
        text = f"Finished 'TrainImagesClassifier' with: {return_code}"

        message_log(text)
        if return_code != 0:
            raise OtbException

        return parameters

    def classify(self, params):
        """Create a gtiff map with each pixel classified as one of the types from our ROI"""

        in_obj = Path(params["io.il"])
        out = in_obj.parent / f"gtt_mask.{self.uid}.tif"
        out_conf = in_obj.parent / f"gtt_mask_conf.{self.uid}.tif"
        parameters = {
            "in": params["io.il"],
            # 'mask' : None,
            "model": params["io.out"],
            "imstat": params["io.imstat"],
            # 'nodatalabel' : 0,
            "out": str(out),
            "confmap": str(out_conf),
            # 'outputpixeltype' : 5
        }

        message_log("<b>Starting image classifier</b>")
        message_log("<b>This might take a while... BE PATIENT</b>")

        folder = Path(iface.gtt.settings["otb/location"])
        self.params = parameters
        return_code = run(folder, "ImageClassifier", parameters, log=self.progress.log.emit, heartbeat=True,)
        text = f"Finished 'ImageClassifier' with: {return_code}"

        message_log(text)
        if return_code != 0:
            raise OtbException

        return parameters
