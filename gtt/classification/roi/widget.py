from pathlib import Path
from collections import OrderedDict

from PyQt5.uic import loadUiType
from PyQt5.QtWidgets import (
    QListWidget,
    QLineEdit,
    QFileDialog,
    QDockWidget,
    QListWidgetItem,
)
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QPixmap, QIcon, QColor

from qgis.utils import iface
from qgis.gui import QgsColorButton

from ... import classification
from ...functions import get_layer, project_folder, message_log

from . import symbol, create, surfaces
from .functions import random_color, create_layer
from ..install import prepare


def install_button(*args):
    """Install OrfeoToolbox button"""
    installed = prepare(False)
    if installed:
        iface.messageBar().pushSuccess("OTB Install", "Succesfully installed")


WIDGET_CONFIG, _ = loadUiType(str(Path(__file__).parent / "roi_widget.ui"))


class RoiWindow(QDockWidget, WIDGET_CONFIG):
    class_added = pyqtSignal(dict)
    class_removed = pyqtSignal(str)
    closeWidget = pyqtSignal(str)

    listWidget: QListWidget
    line_add: QLineEdit
    classes: OrderedDict

    def __init__(self, parent=None, tools=None):
        """Constructor."""
        super().__init__(parent)
        self.setupUi(self)
        self.tools = tools
        self.classes = self.load_classes()
        self.class_added.connect(self.class_add)
        self.class_removed.connect(self.class_remove)
        self._showwarning()

    def closeEvent(self, event):
        self.closeWidget.emit("roi")
        event.accept()

    def _showwarning(self):
        text = (
            "<br/><b>ROI Notes</b><br/>"
            "The ROI based way of making a mask is quite advanced, please read the Advanced-Mask wiki page."
            " The process is streamlined as much as possible, but still requires a brain to get a decent result."
            " If you're impatient ignore this option and just go with color selecting the satmap in your image editor of choice"
        )
        message_log(text)

    def setupUi(self, obj):
        super().setupUi(obj)
        self.verticalLayout.setAlignment(Qt.AlignTop)
        self.button_add.clicked.connect(self.add_class)
        self.button_remove.clicked.connect(self.remove_class)
        self.button_update.clicked.connect(self.update_style)
        self.button_create.clicked.connect(self.create_sample)

        self.color_add.setColor(random_color())  # Set to a new random color

        self.button_otb_existing.clicked.connect(self.existing_button)
        self.button_otb_install.clicked.connect(install_button)

    def existing_button(self, *args):
        """
            Selects BMP file, and turns it into an amazing mask
            Called by the `Import Satmap` button
        """

        options = QFileDialog.Options()
        folder = project_folder()

        data = QFileDialog().getOpenFileName(
            parent=self,
            caption="Select satmap",
            directory=str(folder),
            filter=f"Satmap (*.bmp *.png)",
            options=options,
        )
        file = data[0]
        if file:
            classification.import_bmp(Path(file))

    @pyqtSlot(dict)
    def class_add(self, clas: dict):
        """Adds symbology for given class
        Args:
            clas (dict): contains 'name' and 'color'
        """
        layer = get_layer("gtt_roi")
        if layer is not None:
            symbol.add_symbol(layer, clas["name"], clas["name"], clas["color"])

    @pyqtSlot(str)
    def class_remove(self, name: str):
        """Removes the symbology for given surface type
        Args:
            name (str): surface type to remove
        """
        layer = get_layer("gtt_roi")
        if layer is not None:
            symbol.remove_symbol(layer, name)

    def update_style(self, *args):
        """Synchronizes the listed class in the ROI wizard to the symbology on the gtt_roi layer
        """
        layer = get_layer("gtt_roi")
        if layer is not None:
            symbol.update_style(layer, self.classes)

    def create_sample(self, *args):
        """Activates the polygon creation tool for the currently selected surface type
        """

        layer = create_layer()
        if layer is None:
            return

        selected = self.listWidget.selectedItems()
        if not selected:
            iface.messageBar().pushCritical("ROI Problem", "Select a surface type")
            return

        surface = self.classes["classes"][selected[0].entry]["name"]  # type: str
        create.create(surface)

    def add_class(self, *args):
        """Adds a surface type with name in the lineedit field, plus color select in the QgsColorButton
        """
        self.color_add: QgsColorButton

        text = self.line_add.text()  # type: str
        if not text:
            self.line_add.setPlaceholderText("ENTER CLASSS NAME")
            return

        color = self.color_add.color()  # type: QColor
        key = text.lower()

        self.line_add.setText("")  # Clean the text
        self.color_add.setColor(random_color())  # Set to a new random color
        self.classes["classes"][key] = {"color": color.name(), "name": text}
        entry = self.classes["classes"][key]  # type: dict
        self.add_entry(key, entry)
        self.class_added.emit(entry)
        surfaces.save_settings_file(self.classes)

    def remove_class(self, *args):
        """Removes the currently selected surface type(s)
        """
        selected = self.listWidget.selectedItems()
        if selected:
            for select in selected:
                row = self.lisscaled_heightscaled_height
                tWidget.row(select)
                item = self.listWidget.takeItem(row)
                name = self.classes["classes"][item.entry]["name"]
                del self.classes["classes"][item.entry]
                del item
                self.class_removed.emit(name)

        surfaces.save_settings_file(self.classes)

    def load_classes(self) -> OrderedDict:
        data = surfaces.load_surfaces()
        try:
            data["classes"]
        except KeyError:
            data["classes"] = OrderedDict()

        for key, entry in data["classes"].items():
            self.add_entry(key, entry)
        return data

    def add_entry(self, key: str, entry: dict, select: bool = False) -> QListWidgetItem:
        """Adds surface type to the QListWidget
        Args:
            key (str): Dictionary key for surface class
            entry (dict): Dict containing name and color for surface
        """
        color = entry["color"]
        color_map = QPixmap(30, 30)
        color_map.fill(QColor(color))
        color_icon = QIcon(color_map)
        item = QListWidgetItem(color_icon, entry["name"], parent=self.listWidget)
        item.entry = key

        if select:
            self.listWidget.setCurrentItem(item)
        return item
