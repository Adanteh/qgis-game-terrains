import subprocess
import re
from configparser import ConfigParser
from pathlib import Path

from qgis.core import QgsTask
from qgis.utils import iface
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from .base import Engine, set_tooltip, power_of_two
from ..functions import save_location, qgis_bin_folder, message_log
from .ue4 import HeightmapPng, EngineUE4


class EngineUnity(EngineUE4):
    NAME = "Unity"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def heightResolutionValidate(self, field: QWidget, value: int) -> bool:
        """Checks if the heightmap resolution is valid, will mark the field red if invalid
        
        Args:
            field (QWidget): The widget
            value (int): Value
        
        Returns:
            bool: True if valid
        """

        if not power_of_two(value - 1):
            field.__valid = False
            field.setStyleSheet("background-color: red")
            set_tooltip(
                field, f"{self.NAME} heightmap should be a power of two + 1",
            )
            return False
        else:
            field.__valid = True
            field.setStyleSheet("")
            set_tooltip(field, "")
            return True

    def heightExport(self, path) -> QgsTask:
        """Converts the resampled heightmap to 16bit png"""
        self.height_task = HeightmapRaw("gtt_heightmap", path, iface.gtt.settings)
        return self.height_task


class Int16:
    bottom = -32767
    ceil = 32767
    name = "Int16"


class UInt16:
    bottom = 0
    ceil = 32767
    name = "UInt16"


class HeightmapRaw(HeightmapPng):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def run(self):
        self.export_info(self.filepath)

        output = self.export_raw(self.exportname, self.filepath)  # type: Path
        self.setProgress(100)
        self.output = output
        return True

    def export_raw(self, exportname: str, filepath: Path) -> Path:
        """Exports a 16 bit PNG that's suitable for UE4 engine"""

        output = save_location() / (exportname + ".raw")  # type: Path
        output.parent.mkdir(parents=True, exist_ok=True)
        gdal = qgis_bin_folder() / "gdal_translate.exe"

        file_format = UInt16
        cmd = [
            str(gdal),
            "-ot",
            file_format.name,
            "-of",
            "ENVI",
            "-scale",
            self.min_height,
            self.max_height,
            str(file_format.bottom),
            str(file_format.ceil),
            str(filepath),
            str(output),
        ]

        print(output)
        message_log(f"Exporting heightmap as {output.name}")
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            self.error = result.stderr.decode("utf8")
            return None

        return output
