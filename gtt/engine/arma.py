import subprocess
from pathlib import Path
from numpy import loadtxt, savetxt
from configparser import ConfigParser

from qgis.core import QgsTask
from qgis.utils import iface
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from .base import Engine, power_of_two, set_tooltip
from gtt.functions import save_location, qgis_bin_folder, message_log


class EngineArma(Engine):
    COORD_X = 200000
    COORD_Y = 0
    CRS = "EPSG:32631"
    NAME = "Arma"
    TILED_EXPORT = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def heightResolutionValidate(self, field: QWidget, value: int) -> bool:
        """Checks if the heightmap resolution is valid, will mark the field red if invalid
        
        Args:
            field (QWidget): The widget
            value (int): Value
        
        Returns:
            bool: True if valid
        """

        if not power_of_two(value):
            field.__valid = False
            field.setStyleSheet("background-color: red")
            set_tooltip(
                field, f"{self.NAME} requires the heightmap resolution to be a power of two",
            )

            return False
        else:
            field.__valid = True
            if value >= 8192:
                field.setStyleSheet("background-color: yellow")
                set_tooltip(
                    field, f"{value}px only for expert users. Don't use this unless you know what you're doing",
                )
            else:
                field.setStyleSheet("")
                set_tooltip(field, "")
            return True

    def heightExport(self, path) -> QgsTask:
        """Converts the resampled heightmap to ASC, and adjust the header for arma mapframe"""
        task = HeightmapAsc("gtt_heightmap", path, iface.gtt.settings)
        return task

    def exportLog(self, config: ConfigParser):
        from gtt.extent import extent_middle, wgs_to_utm
        middle_point = extent_middle()
        settings = iface.gtt.settings

        utm = wgs_to_utm(*middle_point, authid=False)
        section = {}
        section["longitude"] = middle_point[1]
        section["latitude"] = middle_point[0] * -1  # Latitude is reversed in arma (lol)
        section["mapSize"] = settings['mapsize']
        section["mapZone"] = utm
        config[f"## {self.NAME} config entries ##"] = section



class HeightmapAsc(QgsTask):
    done = pyqtSignal(object)
    output: Path

    def __init__(self, exportname: str, filepath: Path, settings: dict):
        super().__init__(description="Converting to ASC format")

        self.exportname = exportname
        self.filepath = filepath
        self.settings = settings
        self.output = None
        self.error = None
        self.taskCompleted.connect(self._done)
        self.taskTerminated.connect(self._done)

    def _done(self):
        self.done.emit(self)

    def run(self):
        output = self.export_asc(self.exportname, self.filepath)  # type: Path
        self.setProgress(50)
        if self.isCanceled():
            return False

        output = self.heightmap_arma(output)
        if output is None:
            return False
        self.setProgress(100)
        self.output = output
        return True

    def export_asc(self, exportname: str, filepath: Path) -> Path:
        """Exports an ASC that arma likes"""

        output = save_location() / (exportname + ".asc")  # type: Path
        output.parent.mkdir(parents=True, exist_ok=True)
        gdal = qgis_bin_folder() / "gdal_translate.exe"

        cmd = [
            str(gdal),
            "-a_nodata",
            "0.0",
            "-b",
            "1",
            "-ot",
            "Float32",
            "-of",
            "AAIGrid",
            "-co",
            "FORCE_CELLSIZE=True",
            str(filepath),
            str(output),
        ]

        message_log(f"Exporting heightmap as {output.name}")
        pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = pipe.communicate()
        # Delete some metadta files that make terrain builder import unreliable
        filepathmeta = output.with_suffix(".asc.aux.xml")
        if filepathmeta.exists():
            filepathmeta.unlink()

        filepathmeta = output.with_suffix(".prj")
        if filepathmeta.exists():
            filepathmeta.unlink()

        return output

    def heightmap_arma(self, filepath: Path) -> Path:
        """This handles the exported file according to arma standards"""

        try:
            heightmaparray = loadtxt(str(filepath), skiprows=6)
            size = self.settings["height_reso"]
            cellsize = self.settings["mapsize"] / size

            if self.settings["scaled"]:
                cellsize = cellsize * (self.settings["scaled_size"] / self.settings["mapsize"])

            header = "ncols         %s\n" % size
            header += "nrows         %s\n" % size
            header += "xllcorner     %s.000000\n" % EngineArma.COORD_X
            header += "yllcorner     %s.000000\n" % EngineArma.COORD_Y
            header += "cellsize      %s\n" % cellsize
            header += "NODATA_value  -9999.0"

            if self.settings["scaled_height"]:
                heightmaparray = heightmaparray * (self.settings["scaled_size"] / self.settings["mapsize"])

            savetxt(str(filepath), heightmaparray, header=header, fmt="%1.2f", comments="")
        except Exception as e:
            self.error = e
            return None

        return filepath
