from math import floor
from typing import Tuple
from qgis.core import QgsPoint, Qgis
from qgis.utils import iface

from gtt.functions import get_layer, move_layer_to_top, create_transform_layer, message_log
from gtt.engine.arma import EngineArma

EXTENT = "game terrain tools - map extent"

def extent_error(message="This action requires you to mark target area first"):
    message_log(message, level=Qgis.Critical)
    iface.messageBar().pushCritical("GTT", message)

class NoMarkedAreaLayer(Exception):
    """Marked area layer doesn't exist"""

    def __init__(self, *args, **kwargs):
        extent_error()

def get_working_layer():
    """Gets the current working layer"""
    return get_layer(EXTENT)


def raise_extent():
    """Raises the extent layer to the top"""
    extent = get_working_layer()
    if extent is not None:
        move_layer_to_top(extent)


def transform_extent(targetcrs=EngineArma.CRS) -> list:
    extent_layer = get_working_layer()
    extent = extent_layer.extent()
    if extent_layer.crs().authid() != targetcrs:
        transform = create_transform_layer(extent_layer, target_crs=targetcrs)
        bottom_left = QgsPoint(extent.xMinimum(), extent.yMinimum())
        top_right = QgsPoint(extent.xMaximum(), extent.yMaximum())
        bottom_left.transform(transform)
        top_right.transform(transform)

        return [bottom_left.x(), bottom_left.y(), top_right.x(), top_right.y()]
    return [extent.xMinimum(), extent.yMinimum(), extent.xMaximum(), extent.yMaximum()]


def extent_middle(crs: str = "EPSG:4236") -> Tuple[float, float]:
    """Gets middle point of the extent layer"""
    extent_layer = get_working_layer()
    if extent_layer is None:
        return (0, 0)

    transform = create_transform_layer(extent_layer, crs)
    center = extent_layer.extent().center()
    center = QgsPoint(center.x(), center.y())
    center.transform(transform)
    return (center.y(), center.x())


def extent_utm_zone(authid=True):
    """Gets the UTM zone for the middle of the marker area"""
    extent_layer = get_working_layer()
    if extent_layer is None:
        return EngineArma.CRS

    center = extent_layer.extent().center()
    if extent_layer.crs().authid() != "EPSG:4326":
        transform = create_transform_layer(extent_layer, target_crs="EPSG:4326")
        center = QgsPoint(center.x(), center.y())
        center.transform(transform)
    return wgs_to_utm(center.y(), center.x(), authid=authid)


def wgs_to_utm(lat, lon, authid=False):
    """Converts EPSG:4326 coordinates into UTM zone or authid number"""
    utm_zone = floor((lon + 180) / 6) + 1

    if lat >= 56.0 and lat < 64.0 and lon >= 3.0 and lon < 12.0:
        utm_zone = 32

    if lat >= 72.0 and lat < 84.0:
        if lon >= 0.0 and lon < 9.0:
            utm_zone = 31
        elif lon >= 9.0 and lon < 21.0:
            utm_zone = 33
        elif lon >= 21.0 and lon < 33.0:
            utm_zone = 35
        elif lon >= 33.0 and lon < 42.0:
            utm_zone = 37

    south = lat < 0
    name = f"UTM {utm_zone}{('N', 'S')[south]}"

    if authid:
        authid_ = 32600 + utm_zone
        if south:
            authid_ += 100
        return f"EPSG:{authid_}"
    return utm_zone
