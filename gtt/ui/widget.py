import os
import json
from functools import partial
from typing import Dict

from PyQt5.QtWidgets import QDockWidget, QRadioButton, QComboBox
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QTextCursor

from gtt.heightmap.downloader import heightmap_sources, download_heightmap
from gtt.osm.download import download_osm
from gtt.functions import logger, FOLDER
from gtt.functionsmisc import clear_cache
from gtt.engine import engines
from gtt import classification
from gtt.satmap import add_satmap
from .functions import show_zoom_level, add_log


from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from gtt.tools import GttTools



WIDGET_CONFIG, _ = loadUiType(os.path.join(os.path.dirname(__file__), "widget.ui"))


class GttWidget(QDockWidget, WIDGET_CONFIG):

    closeWidget = pyqtSignal(str)
    tools: "GttTools"

    def __init__(self, parent=None, tools=None):
        """Constructor."""
        super().__init__(parent)

        self.setupUi(self)
        self.tools = tools
        self.initOptions()

    def closeEvent(self, event):
        self.closeWidget.emit("primary")
        event.accept()

    def initOptions(self):
        self.area_button.clicked.connect(self.tools.mark_area)
        self.export_button.clicked.connect(self.actionExport)

        self.initSatmapSources()

        self._load_tabwidget("primary_tabs")
        self._load_spinbox("mapsize")
        self._load_spinbox("tilesize")
        self._load_check_button("tilesize_enabled")
        self._load_spinbox("map_reso")
        self._load_spinbox("height_reso")
        self._load_spinbox("zoom_level")
        self._load_check_button("scaled")
        self._load_check_button("scaled_height")
        self._load_check_button("lower_to_zero")
        self._load_check_button("roads_raw")
        self._load_spinbox("scaled_size")

        # Exports
        self._load_check_button("export_satmap")

        self._load_check_button("export_mask")
        self.initMaskExports()

        self._load_check_button("export_roads")
        self._load_check_button("export_roads_raster")
        self._load_check_button("export_shapes")
        self._load_check_button("export_height")
        logger.message.connect(self.add_text)

        classification.ui.initUI(self)

        # Height
        self.initHeightmapSources()

        # Roads
        self.road_download.clicked.connect(download_osm)

        # Misc
        self.cache_clear.clicked.connect(clear_cache)
        self.tools.exporter.progressChanged.connect(self.export_progressbar.setValue)
        self.button_zoom_level.clicked.connect(show_zoom_level)
        self.button_file_logging.clicked.connect(add_log)
        self.button_load_settings.clicked.connect(self.tools.set_settings_from_extent)

        self._load_engine("engine", options=engines)

    def initMaskExports(self):
        self.radio_mask_osm: QRadioButton
        if self.tools.settings["mask_type"] == "osm":
            self.radio_mask_osm.setChecked(True)
        else:
            self.radio_mask_otb.setChecked(True)

        self.radio_mask_osm.toggled.connect(partial(self._toggled_radio, "mask_type", "osm"))
        self.radio_mask_otb.toggled.connect(partial(self._toggled_radio, "mask_type", "otb"))

    def initSatmapSources(self):
        options_path = FOLDER / "satmap" / "satmap.json"
        with options_path.open(mode="r") as options_file:
            satmap_options = json.load(options_file)  # type: Dict

        combo = self._load_combo("satmap", satmap_options.keys())
        self.button_sat_add.clicked.connect(partial(self._add_satmap, combo))

    def initHeightmapSources(self):
        combo = self._load_combo("heightmap", heightmap_sources.keys())
        self.button_height_add.clicked.connect(partial(self._add_heightmap, combo))

    def add_text(self, text):
        cursor = self.log_widget.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertHtml(text + "<br>")
        self.log_widget.setTextCursor(cursor)
        self.log_widget.ensureCursorVisible()

    def _add_satmap(self, combo):
        source = combo.currentText()
        add_satmap(source)

    def _add_heightmap(self, combo):
        source = combo.currentText()
        download_heightmap(source)

    def actionExport(self):
        """Any items added to exporttypes here will be handled by GttExport._continue"""
        exporttypes = []
        if self.tools.settings["export_satmap"]:
            exporttypes.append("satmap")
        if self.tools.settings["export_mask"]:
            if self.tools.settings["mask_type"] == "osm":
                exporttypes.append("mask")
            else:
                exporttypes.append("classified mask")

        if self.tools.settings["export_height"]:
            exporttypes.append("height")
        if self.tools.settings["export_roads"]:
            exporttypes.append("roads")
        if self.tools.settings["export_roads_raster"]:
            exporttypes.append("roads_raster")
        if self.tools.settings["export_shapes"]:
            exporttypes.append("shapes")
        
        self.tools.export(exporttypes)

    def _load_combo(self, name, options=[]):
        """Loads a combobox (Dropdown box that opens with multiple text options)"""
        widget = getattr(self, "combo_" + name)
        widget.addItems(options)
        if self.tools.settings[name] in options:
            widget.setCurrentText(self.tools.settings[name])
        widget.currentTextChanged.connect(partial(self.__setting_changed, widget, name))
        return widget

    def _load_engine(self, name, options=[]):
        """Loads a combobox (Dropdown box that opens with multiple text options)"""

        widget = getattr(self, "combo_" + name)  # type: QComboBox
        for index, engine in enumerate(options):
            widget.addItem(engine.NAME, userData=engine)
            if self.tools.settings[name] == engine.NAME:
                widget.setCurrentIndex(index)
                self.__engine_selected(widget, index)

        widget.currentIndexChanged.connect(partial(self.__engine_selected, widget))
        widget.currentTextChanged.connect(partial(self.__setting_changed, widget, name))
        return widget

    def __engine_selected(self, widget: QComboBox, index: int):
        engine = widget.itemData(index)
        engine.heightResolutionValidate(self.spin_height_reso, self.spin_height_reso.value())
        self.tools.engine = engine

    def _toggled_radio(self, setting: str, value: str, state: bool):
        if state:
            self.__setting_changed(None, setting, value)

    def _load_tabwidget(self, name):
        widget = getattr(self, "tab_" + name)
        widget.setCurrentIndex(self.tools.settings[name])
        widget.currentChanged.connect(partial(self.__setting_changed, widget, name))

    def _load_check_button(self, name):
        """Loads a checkbutton with a setting, and adds connection to change setting for it in profile"""
        widget = getattr(self, "check_" + name)
        widget.setCheckState((0, 2)[self.tools.settings[name]])
        widget.stateChanged.connect(partial(self.__setting_changed, widget, name))

    def _load_spinbox(self, name):
        """Loads a number spinbox"""
        widget = getattr(self, "spin_" + name)
        widget.setValue(self.tools.settings[name])
        widget.valueChanged.connect(partial(self.__setting_changed, widget, name))

    def __setting_changed(self, widget, key, state):
        self.tools.setting_set(key, state)
        print(f"Setting {key} changed to {state}")
        if key == "height_reso":
            self.tools.engine.heightResolutionValidate(widget, state)
