import threading
from pathlib import Path

from PyQt5.QtWidgets import QDockWidget
from PyQt5.uic import loadUiType
from PyQt5.QtCore import pyqtSignal
from qgis.utils import iface
from qgis.core import QgsSettings

from gtt.static import SETTINGSNAME
from gtt.functions import message_log
from gtt.objects.functions import ask_library_path
from gtt.objects.load_tb import import_file
from gtt.objects.library import TbLibraryCollection

WIDGET_CONFIG, _ = loadUiType(str(Path(__file__).parent / "objects_widget.ui"))

def obj_widget(*args):
    iface.gtt_plugin.widgetCreate("objects", ObjectsWidget)


class ObjectsWidget(QDockWidget, WIDGET_CONFIG):

    closeWidget = pyqtSignal(str)
    library_loaded = pyqtSignal()
    library_path: Path

    def __init__(self, parent=None):
        """Constructor."""
        super().__init__(parent)

        self.library = None
        self.library_path = None
        self.loading_library = False

        self.setupUi(self)
        self.initOptions()

    def closeEvent(self, event):
        self.closeWidget.emit("objects")
        event.accept()

    def initOptions(self):
        self.button_import.clicked.connect(self._import_file)
        self.button_library_path.clicked.connect(self._set_library_path)

        settings = QgsSettings()
        dir_last = settings.value(f"{SETTINGSNAME}/objects/library_path", None, type=str)

        if dir_last != "" and Path(dir_last).is_dir():
            self.library_path = Path(dir_last)
            self._load_library()

    def _import_file(self, *args):
        if self.library_path is None:
            iface.messageBar().pushCritical("OBJECTS", "Please first set the library path")
            return

        if self.loading_library:
            iface.messageBar().pushCritical(
                "OBJECTS", "Please wait a couple seconds for the library to finish loading",
            )
            return

        import_file(self.library)

    def _set_library_path(self, *args):
        """Handles setting the library path"""
        settings = QgsSettings()
        dir_last = settings.value(f"{SETTINGSNAME}/objects/library_path", None, type=str)
        if dir_last == "" or not Path(dir_last).is_dir():
            path = Path.cwd()
        else:
            path = Path(dir_last)

        location = ask_library_path(path)
        if location != "":
            settings.setValue(f"{SETTINGSNAME}/objects/library_path", location)
        self.library_path = Path(location)
        self._load_library()

    def _load_library(self):
        thread = threading.Thread(target=self.load_library)
        thread.start()

    def load_library(self):
        self.loading_library = True
        self.library = TbLibraryCollection(self.library_path)
        self.loading_library = False
        message_log("Finished loading library")
