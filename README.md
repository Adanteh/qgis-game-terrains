# GameTerrainTools
This is a QGIS plugin, meant to get source files used for virtual terrain creation,
based on real world dat. Primary target is Arma3 and DayZ, more games will be added in future.
This is not an editing program, use [L3DT](http://www.bundysoft.com/L3DT/) for that. 

The basic usage consists of getting easily accessible and globally available data,
however because it's a QGIS plugin, you can add whichever other sources you want,
and have this plugin take care of your resampling/translation/required formatting needs

[Video Link](https://www.youtube.com/watch?v=Ap0X3JpJzb4)

## Discord
For general questions, suggestions, problems please use the QGIS_PLUGIN channel
in [this discord](https://discord.gg/Ysq7HcK)

## Important info for beginners
This is the basic usage of **this plugin**, if you want to know how QGIS works, read a QGIS guide. It's highly recommend to first export with default settings (Maybe adjusting to the Engine you want), seeing which files come out, and how to import them in the engine of your choice.

Only start adjusting values if you actually understand them and reading this **entire** README.

## Usage
For more arma-centric terms in regards to mapframes check the general notes. For basic usage I recommend
using the OSM mask option, for the [regions of interest](Advanced-Mask) method please check the guide. 
It will have **far** better results, but does require actually reading the guide and patience to use it.

* Add a satmap source (Find one yourself or select a source and press Add Satmap)
* Save your QGIS project somewhere (Ctrl+S)
* Set map size (Width/Height in meters)
* click Mark Area, left click map to select that area
* Download Heightmap
* Download Roads (Also required for mask export!)
* Check the exports you want
* Click export
* **BE PATIENT**
* Profit

## Download
{{check_wiki_for_download_link}}
Created at: {{time}}

[Check changelog here](changelog)

## Enfusion (Arma Reforger)
For Enfusion specific info please check [the Enfusion page](enfusion).
This does not contain all info, it's still important to understand the basics of this page!

## Install
Plugins on top menu > Manage and Install Plugins > Install from Zip > select gameterraintools.zip

## General notes
All resulting exports go in a `gtt_export` folder located at your QGIS project location 
A file `gtt_export.txt` will contain info  with corresponding values for your Mapframe properties

**Map Area** is the only thing deciding the size (in meters) of your terrain.
**Scaling** is the size for export. Marking 20km, then checking scaling and setting it to 15km,
means that everything will be scaled down 25%. There is absolutely no point on setting Scaling to be the same as your marked area! (I've seen way too many screenshots with this)

You can add your own files, and let the plugin handle the processing (Fixing resolutions, making compatible for engine)
by dragging in the files and then renaming the layer (Select layer and press F2) to what's listed below.

#### Satmap
* Single layer `gtt_satmap` is used for this. Any custom sources should be given this layer name.
* Resolution is the total resolution you want your satmap (and mask) to be. Use `-1` to ignore (1m/px) (Recommended)
* For large satmaps (20k and up) you should check `Tiling` and set the size in pixels for each tile. **this is before resolution / scaling change if used**. Tiling is not supported by Enfusion.

#### Heightmap
* **All** `gtt_heightmap` layers are used. **Cleanup ones you dont want in**
* The `gtt_heightmap`s will be merged automatically. 
* `Scale vertical height` will change the height range by the same % as overal scaling. I recommend keeping this turned off.
* `Lower terrain to 0m` will lower the entire terrain, so it doesn't "float". Only used for Enfusion.
* If a manually added source does not export properly, report issues!

#### Roads
* Single layer `osm lines` is used, you can make manual adjustments before export (For example which ID to apply).

#### Mask 
* This currently does not do any checking on allowed surface per tile (6 per tile for arma)
* Single layer `osm multipolygons`, you can make manual adjustments before export
* The Shape based implementation is heavily dependent on available data for that area
