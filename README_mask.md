
## Mask - Surface Classification
This uses special algorhitms to create a surface mask out of a satmap, plus some marked 
Regions of Interest (ROI). It requires a little bit of manual work and some patience, but should give good results
This is dependent on an exported satmap (Make sure you check Export Satmap, or have a gtt_satmap.bmp from previous export)
It requires you to make basic shapes (ROI) to indicate the sort of surface the marked area is.

**IT CURRENTLY DOES NOT WORK WITH TILED SATMAPS** 

**Please read the Notes at least once so you have a better idea of what to do** 

## Basic Usage
* Install OrfeoToolbox (Select folder where it should be installed, can be anywhere)
* Click `Create Regions of Interest`
* Create some surface types you want to mark in the opened widget
  * Select a color
  * Type in a UNIQUE name
  * Press Add Type
* Create a couple samples for each surface type
  * Select a surface type
    * If there is no sample for a surface in the list, it will get ignored
    * You only need 2 or so samples per surface
  * Press Create Sample
  * Left click to add a point on the map, right click to finish the shape
    * Keep shapes simple
    * It is usually better to keep shapes a bit smaller than the actual area you're marking
* You might need to click `Update Style` to get colors showing up properly on the map
* Run Export as normal
* **BE PATIENT**
* Have a drink
* **BE PATIENT**
* Get some exercise
* **BE PATIENT**

#### Notes
* Amount and size of samples, plus the overal satmap size change the length this takes by **A LOT**
* Don't go crazy adding tons of large samples straight away, start with like 10 smaller samples **in total** and give it a test run

* This does not handle limiting to 5 unique surfaces per tile (Arma / Similar engines). 
If you don't want to refine this mask yourself, make sure you don't use more than 5 surface types in total!

* The main algorithm has no way of telling progress and it can take a long time (Easily an hour for larger maps)
It is also quite heavy on the CPU, so prepare to take a little PC break when running it

* This is still mostly dependent on colors, if your concrete squares  are the same color as a riverbank/beach,
you might have to just refine the mask yourlsef in GIMP. There is no way for me to fix this.

* The list of surfaces in the widget is global for your computer, don't bother deleting ones you're not using in the current project

* The Import Satmap option is to be used in 100% clean projects (No other satmap, no extent, no heightmap etc)
It's there to allow classifying completely independent of geolocation

## Sampling
I recommend first testing some samples with a smaller area, to get a feel for how to mark things
Below are some samples, with the first two pictures giving quite bad results (Water everywhere)

![Bad Sample](uploads/7239581177394ac12ff92ef67e8b4a9f/sample1.png)
![Bad Sample Result](uploads/bfa43821bc55f9cccde3e50bb08b8369/sample1_result.png)

Adding two more samples made the result a lot better
![Good Sample](uploads/63982ceaf5d463cfc124ae6b7f1aa4f8/sample2.png)
![Good Sample Result](uploads/afb70acb85aa59675ede972565bc83d3/sample2_result.png)