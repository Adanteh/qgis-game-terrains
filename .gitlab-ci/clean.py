"""Rando dev script to cleanup up entire project CI"""
from api.pipeline import clean_pipeline, clean_releases, clean_jobs
from functions import load_environment

if __name__ == "__main__":
    load_environment()
    clean_pipeline()
    clean_jobs()
    clean_releases()
