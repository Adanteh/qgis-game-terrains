import datetime
from pathlib import Path
from typing import List

from api.wiki import update_page
from functions import get_latest_release_tag

FOLDER = Path(__file__).parent


def update_wiki_release(files: List[str]):
    """
    Updates the wiki page.
    It will take the contents of README.md and puts them on the landing page of the Repos wiki
    It will also put the URLs for the download into the page.

    """
    print("## Updating wiki home page")
    print("Files:")
    print(files)
    with (Path(__file__).parents[1] / "README.md").open() as fp:
        data = fp.read()

    now = datetime.datetime.now()
    time = str(now.strftime("%m-%d-%y %H:%M:%S"))
    data = data.replace("{{check_wiki_for_download_link}}", files)
    data = data.replace("{{time}}", time)

    response = update_page("home", data)
    if response is None:
        print("ERROR: Failed to update wiki home page")


def update_wiki_version():
    version = get_latest_release_tag()
    update_page("version", version)
