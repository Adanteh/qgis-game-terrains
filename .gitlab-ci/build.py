"""
Primary build script. Collect all required files and puts them in a .zip file QGIS understands
"""

from pathlib import Path

import zipfile
import sys
import configparser
from functions import get_latest_release_tag, iterfiles


def write_version_to_metadata(source: Path):
    """Updates the metadata file with release tag (From CI_COMMIT_TAG environment variable)"""
    latest_tag = get_latest_release_tag()
    if latest_tag is None:
        raise Exception("Can't write version to metadata.txt. No valid release tag found")

    version = latest_tag.strip("release/")
    if version.startswith("v"):
        version = version[1:]

    file: Path = source / "metadata.txt"
    if not file.exists():
        raise FileNotFoundError(f"Can't find metadata.txt file: {file.absolute()}")

    config = configparser.ConfigParser()
    config.read(file)
    config["general"]["version"] = version
    with file.open(mode="w") as fp:
        config.write(fp)

    print(f"Wrote version '{version}' to metadata.txt")


def create_zip(source: Path, filename: str = "release"):
    """Zips a given source, puts in in the source/assets folder

    Args:
        source (Path): Folder to pack
        filename (str, optional): Filename for the zip, without extension. Defaults to "release".
    """
    target = source.parent / "assets" / f"{filename}.zip"
    target.parent.mkdir(parents=True, exist_ok=True)
    print(f"Creating source from: {source.resolve()}")
    print(f"Creating zip to: {target.resolve()}")

    files = source
    print(f"Writing file {target.name}")
    with zipfile.ZipFile(str(target), mode="w") as zf:
        for file in iterfiles(files, top=True):
            zf.write(str(file), str(filename / file.relative_to(files)), zipfile.ZIP_DEFLATED)

    print("Finished writing file")
