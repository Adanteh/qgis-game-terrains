import subprocess
import os
from pathlib import Path
import yaml


def load_environment(release_tag: str = None):
    """Loads in environment.yml file if it exists, allows for deployment from local

    Args:
        release_tag (str, optional): Release tag. Defaults to CI_COMMIT_TAG.

    Raises:
        FileNotFoundError: .enviroment.yml does not exist
    """
    env_file: Path = Path(__file__).parent / ".environment.yml"
    if env_file.exists():

        with env_file.open(mode="r") as fp:
            data = fp.read()

        variables = yaml.safe_load(data)
        for key in variables:
            print(f"{key} set to {str(variables[key])}")
            os.environ[key] = str(variables[key])
    else:
        raise FileNotFoundError("Missing .environment.yml, required for local release creation")

    if release_tag is None:
        latest_tag = get_latest_release_tag()
        if latest_tag is None:
            raise Exception("Could not find suitable release tag. See get_latest_release_tag")
        os.environ["CI_COMMIT_TAG"] = latest_tag
    else:
        os.environ["CI_COMMIT_TAG"] = release_tag

    print(f"Set CI_COMMIT_TAG to {os.environ["CI_COMMIT_TAG"]}")


def get_latest_release_tag() -> str | None:
    """
        Gets the latest release tag of this CI_COMMIT or through Git.
        Note that the git version only works for the local_build
        This gets the full tag name (Including any prefixes)
    """

    version = os.getenv("CI_COMMIT_TAG", None)
    if version is not None:
        return version
    
    result = subprocess.run(["git", "tag", "--sort=v:refname"], stdout=subprocess.PIPE, shell=True)
    if result.returncode != 0:
        print(result.returncode, result.stderr)
        return None

    tags = result.stdout.decode("utf8").split("\n")[:-1]
    filtered = list(filter(lambda x: x.startswith("release/"), tags))
    if not filtered:
        print("No tag found starting with 'release/'")
        return None

    return filtered[-1]


def iterfiles(path: Path, top: bool = False, exclude=[]):
    """Recursive file iterator with exclude list"""

    if top:
        yield path

    for child in path.iterdir():
        child: Path
        if child.name not in exclude:
            if child.is_dir():
                yield child
                for file in iterfiles(child):
                    yield file
            else:
                yield child
