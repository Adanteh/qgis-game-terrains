import requests
import os

from typing import TYPE_CHECKING, Union
if TYPE_CHECKING:
    from requests.sessions import Session
    from requests import Response

def create_session() -> "Session":
    """Creates a requests session. Requires GITLAB_API_KEY to be register"""
    api_key = os.getenv("GITLAB_API_KEY", None)
    if api_key is None:
        raise Exception("GITLAB_API_KEY is not set in environment. Can not start session");

    session = requests.session()
    session.headers.update({"PRIVATE-TOKEN": api_key})
    return session


def create_page():
    """Test function to create new wiki page from API call"""
    project_id = os.getenv("CI_PROJECT_ID", None)
    url_base = os.getenv("GITLAB_API_URL", "https://gitlab.com/api/v4/")
    url_wiki = f"{url_base}projects/{project_id}/wikis"
    data = {
        "title": "Test",
        "slug": "test",
        "format": "markdown",
        "content": "Test Page",
    }
    session = create_session()
    session.post(url_wiki, data=data)


def page_exists(session: "Session", url: str) -> bool:
    """Check if the given wiki page exists"""
    response = session.get(url)
    return response.status_code == 200


def update_page(title: str, content: str, format="markdown") -> Union["Response", None]:
    project_id = os.getenv("CI_PROJECT_ID", None)
    url_base = os.getenv("GITLAB_API_URL", "https://gitlab.com/api/v4/")
    slug = title.lower()
    url_wiki = f"{url_base}projects/{project_id}/wikis/{slug}"

    session = create_session()
    if not page_exists(session, url_wiki):
        data = {
            "title": title,
            "id": slug,
            "format": "markdown",
            "content": content,
        }
        url_wiki = f"{url_base}projects/{project_id}/wikis"
        response = session.post(url_wiki, data=data)
    else:
        data = {
            "title": title,
            "slug": slug,
            "format": "markdown",
            "content": content,
        }
        response = session.put(url_wiki, data=data)
    return response

