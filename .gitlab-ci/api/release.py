"""

MIT License

Copyright (c) 2019 Devio Security / tools

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""

from pathlib import Path
from typing import Tuple, List
from pprint import pprint
import subprocess
import requests
import logging
import yaml
import os
import sys

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
CI_PATH = Path(__file__).resolve().parent

# https://docs.gitlab.com/ce/api/projects.html#upload-a-file
# https://docs.gitlab.com/ee/api/releases/#create-a-release
# https://docs.gitlab.com/ee/ci/variables/#syntax-of-environment-variables-in-job-scripts

yamlBlacklist = [
    "image",
    "variables",
    "services",
    "before_script",
]

cienvs = [
    "CI_SERVER_VERSION_MAJOR",
    "CI_SERVER_VERSION_MINOR",
    "CI_SERVER_VERSION_PATCH",
    "CI_RUNNER_DESCRIPTION",
    "CI_PROJECT_NAMESPACE",
    "CI_REGISTRY_PASSWORD",
    "CI_COMMIT_SHORT_SHA",
    "CI_COMMIT_REF_NAME",
    "CI_SERVER_REVISION",
    "CI_REPOSITORY_URL",
    "CI_REGISTRY_IMAGE",
    "CI_SERVER_VERSION",
    "GITLAB_USER_EMAIL",
    "CI_JOB_TRIGGERED",
    "CI_REGISTRY_USER",
    "CI_PIPELINE_IID",
    "CI_PAGES_DOMAIN",
    "CI_PROJECT_NAME",
    "CI_PROJECT_PATH",
    "CI_PIPELINE_ID",
    "CI_PROJECT_DIR",
    "CI_PROJECT_URL",
    "CI_RUNNER_TAGS",
    "CI_SERVER_NAME",
    "GITLAB_USER_ID",
    "CI_COMMIT_SHA",
    "CI_COMMIT_TAG",
    "CI_JOB_MANUAL",
    "CI_PROJECT_ID",
    "CI_JOB_STAGE",
    "CI_JOB_TOKEN",
    "CI_PAGES_URL",
    "CI_RUNNER_ID",
    "CI_JOB_NAME",
    "CI_REGISTRY",
    "CI_JOB_ID",
    "CI_SERVER",
]


class GitlabReleaseApi(object):
    def __init__(self, url: str, projectId: int, apiKey: str):
        # init session and api key
        self.s = requests.session()
        self.s.headers.update({"PRIVATE-TOKEN": apiKey})

        # project id we are currently working on
        self.projectId = projectId

        # set our url e.g. https://gitlab.com/api/v4
        self.api_url = url
        if self.api_url[-1:] == "/":
            self.api_url = self.api_url[:-1]

        # all our endpoints
        self.api_upload = "{}/projects/{}/uploads".format(self.api_url, self.projectId)
        self.api_releases = "{}/projects/{}/releases".format(self.api_url, self.projectId)

    def fileExists(self, path):
        return os.path.isfile(path)

    def dirExists(self, path):
        return os.path.isdir(path)

    def filesDir(self, path):
        if not self.dirExists(path):
            logging.error("directory does not exist")
            return False
        files = []
        for r, d, f in os.walk(path):
            for file in f:
                files.append(os.path.join(r, file))
        return files

    def uploadFile(self, path) -> dict:
        if not self.fileExists(path):
            logging.error(f"File {path} does not exist")
            return False

        files = {"file": open(path, "rb")}
        response = self.s.post(self.api_upload, files=files)
        if response.status_code == requests.codes.unauthorized:
            msg = "Create a gitlab API token, and add it to CI variables for access"
            logging.error(f"{response.status_code} - Unauthorized.")
            logging.error(msg)
            raise Exception(response.content)

        print(f"Uploaded file {path}")
        return response.json()

    def uploadDir(self, path) -> List[dict]:
        if not self.dirExists(path):
            logging.error(f"Dir {path} does not exist")
            return False

        files = self.filesDir(path)

        responses = []
        for file in files:
            responses.append(self.uploadFile(file))

        return responses

    def parseYaml(self, yaml_path: Path, working_dir: Path):
        """.gitlab-ci-release.yml path"""
        if not self.fileExists(yaml_path):
            logging.error(f"yaml {yaml_path} does not exists")
            return (None, None)

        with open(yaml_path, mode="r") as fp:
            data = fp.read()

        release = yaml.safe_load(data)

        # make sure we have the required stuff
        if "name" not in release:
            logging.error("conf: need name for our release!")
            return (None, None)
        elif "tag_name" not in release:
            logging.error("conf: need tag_name for our release!")
            return (None, None)
        elif "description" not in release:
            logging.error("conf: need description for our release!")
            return (None, None)

        # fix our environment variables
        for key in release:
            value = release[key]

            if type(value) is not str:
                continue

            for cienv in cienvs:
                value = value.replace("$" + cienv, os.getenv(cienv, "ERRORNOTSET"))
                value = value.replace("${" + cienv + "}", os.getenv(cienv, "ERRORNOTSET"))
            release[key] = value

        name = release["name"]
        tag_name = release["tag_name"]
        description = release["description"]
        base_url = release["base_url"]

        assets_dir = None
        if "assets_dir" in release:
            assets_dir = working_dir / release["assets_dir"]

        changelog = False
        if "changelog" in release:
            changelog = release["changelog"]

        ref = None
        if "release_ref" in release:
            ref = release["release_ref"]

        print("\n## YAML Contents ##")
        print(
            f"{'name:':20} {name}\n"
            f"{'tag_name:':20} {tag_name}\n"
            f"{'description:':20} {description}\n"
            f"{'assets_dir:':20} {assets_dir}\n"
            f"{'changelog:':20} {changelog}\n"
            f"{'base_url:':20} {base_url}\n"
        )

        description = self.make_description(description, changelog=changelog)
        if description is None:
            return {"response": None}
        return self.make_release(name, tag_name, description, assets=assets_dir, base_url=base_url)

    def make_description(self, description: str, changelog=False) -> str:
        if description:
            if self.fileExists(description):
                with open(description, mode="r") as fp:
                    description = fp.read()
        else:
            description = ""

        # if changelog is needed, then lets do that!
        if changelog:
            if not os.getenv("GITCHANGELOG_CONFIG_FILENAME", None):
                os.environ["GITCHANGELOG_CONFIG_FILENAME"] = str(
                    Path(__file__).resolve().parents[1] / ".gitchangelog.rc"
                )

            result = subprocess.run(["gitchangelog"], stdout=subprocess.PIPE, shell=True)
            if result.returncode == 0:
                outChangelog = result.stdout.strip()
            else:
                print("ERROR: Gitchangelog")
                print(result.returncode, result.stderr)
                return None
            description += outChangelog.decode("utf8")
        return description

    def make_release(
        self, name: str, release_tag: str, description: str, ref=None, assets: Path | None = None, base_url=None
    ):
        """Creates the release

        Args:
            name (str): General release name
            tag_name (str): Git tag to create release from
            description (str): General description
            ref (_type_, optional): Reference to commit. Required if tag_name does not exist. Defaults to None.
            assets_dir (list, optional): Assets directory or file to attach to the release. Defaults to None.
            base_url (_type_, optional): URL to add in front of download links (Generally just the road gitlab url). Defaults to None.

        Returns:
            _type_: _description_
        """
        changelog = "## Changelog\n" + description
        uploaded_files: List[dict] = []

        # Upload any files from the asset dir.
        if assets is not None and assets.exists():
            print("## Uploading files ##")
            if assets.is_file():
                uploaded_files.append(self.uploadFile(str(assets)))
            elif self.dirExists(assets):
                uploaded_files = self.uploadDir(str(assets))
            else:
                logging.error("unknown format for uploads or no such dir/file")
                return {"response": None}

        # Add the uploaded files to the assets
        asset_links = []
        files_markdown = ""
        if len(uploaded_files) > 0:
            print("\n## Uploaded files")

            links = "## Download\r\n"
            for file in uploaded_files:
                # One of the properties present on the upload response is markdown, but this gives us an invalid link
                # Instead we need to use the full_path for any valid URLs (Including for asset uploads)
                absolute_file_link = file["full_path"]
                if base_url is not None:
                    absolute_file_link = base_url + absolute_file_link

                asset_links.append({"name": file["alt"], "url": absolute_file_link})

                # Prepare the links for usage in the release description
                markdown = f"[{file["alt"]}]({absolute_file_link})"
                links += f"{markdown}\n"
                files_markdown += f"{markdown}\n"
                print(f"File link for '{file["alt"]}': {absolute_file_link}")

            # Added to the script is a header with all the URLs.
            description = links + "\n" + description

        data = {
            "name": name,
            "tag_name": release_tag,
            "description": description,
        }

        # if asset_links:
        #     data["assets"] = { "links": asset_links }

        # This is required if the tag_name is not present on remote branch
        if ref is not None:
            data["ref"] = ref

        print("\n## Creating release ##")
        print(f"Release name: {name}")
        print(f"Release tag: {release_tag}")
        response = self.s.post(self.api_releases, data=data)

        # Attach an asset to the release. We do this separately because attaching it to the release straight away does not seem to work.
        # It always spits out a "400 Assets is invalid"
        if asset_links and response.status_code == 201:
            print("## Attaching assets to release")

            # Remove any slashes from the release tag. Else you get a 404 on the API link
            release_tag_clean = release_tag.replace("/", "%2F")
            assets_url = f"{self.api_releases}/{release_tag_clean}/assets/links"
            asset_data = asset_links[0]
            asset_response = self.s.post(assets_url, data=asset_data)
            if asset_response is None or asset_response.status_code != 201:
                print("Failed to attached assets")
                print(asset_response.text)

        return {
            "response": response,
            "data": data,
            "files": files_markdown,
            "changelog": changelog,
        }


def create_release(yaml_file: Path, working_dir: Path) -> Tuple[bool, dict]:
    """
    Creates a new release on gitlab
    """
    url = os.getenv("GITLAB_API_URL", "https://gitlab.com/api/v4/")
    project_id = os.getenv("CI_PROJECT_ID", None)
    api_key = os.getenv("GITLAB_API_KEY", None)

    if project_id is None:
        msg = "CI_PROJECT_ID is not send in enviroment"
        logging.error(msg)
        raise Exception(msg)

    if api_key is None:
        logging.error("Create a gitlab API token, and add it to CI variables for access")
        raise Exception("Please enter GITLAB_API_KEY in CI Variables for this repo")

    api = GitlabReleaseApi(url, project_id, api_key)
    result = api.parseYaml(str(yaml_file), working_dir)
    response = result["response"]

    success = False
    if response is None:
        print("## Failed to make release ##")
        print("Unknown error")
    elif response.status_code != 201:
        print("## Failed to make release ##")
        print(response.text)
    else:
        print("## Made release ##")
        success = True

    print("\n")
    return (success, result)
