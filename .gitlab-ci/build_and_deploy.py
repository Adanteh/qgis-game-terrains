"""Script to build locally"""

import sys
from pathlib import Path
from argparse import ArgumentParser

from functions import load_environment
from build import create_zip, write_version_to_metadata
from deploy import update_wiki_release, update_wiki_version
from api.release import create_release

FOLDER = Path(__file__).parent


class Builder:
    """
    Building process. Can still publish a release.
    This uses the latest release tag version already present on git (starting with release/)
    """

    def __init__(self, source: Path, filename: str, local: bool = False):
        self.source = source
        self.filename = filename

        # Loads in environment variables. Normally we expect these to be set by gitlab's CI
        if local:
            load_environment()

    def build(self):
        write_version_to_metadata(self.source)
        create_zip(self.source, filename=self.filename)

    def create_release(self):
        success, result = create_release(FOLDER / ".gitlab-ci-release.yml", FOLDER.parent)

        if success and result:
            files = result["files"]
            update_wiki_release(files)
            update_wiki_version()


if __name__ == "__main__":
    """Call with `.build_and_deploy.py <SOURCE NAME>"""

    parser = ArgumentParser()
    parser.add_argument("name")
    parser.add_argument("--local", action="store_true", default=False)
    args = parser.parse_args()

    source = Path(__file__).parent.parent / args.name
    builder = Builder(source, args.name, args.local)
    builder.build()
    builder.create_release()
